from collections import OrderedDict, Counter
from enum import Enum
from json import loads
from math import ceil, floor, sqrt
from pathlib import Path
from random import Random

import numpy as np
from matplotlib import pyplot, style
import seaborn
from pandas import read_csv, DataFrame, Series
from sklearn.metrics import roc_curve, auc, precision_recall_curve, average_precision_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MultiLabelBinarizer, OneHotEncoder
import tensorflow as tf
import tensorflow_addons as tfa
import kerastuner as kt
from tensorflow_hub import KerasLayer

BASE_RESULTS_DIR = f'results/_current'
BASE_DATA_DIR = f'data'
POSTER_DIR = f'{BASE_DATA_DIR}/images/w342'
MOVIES_CSV_PATH = f'{BASE_DATA_DIR}/movies-clean.csv'
GENRES_CSV_PATH = f'{BASE_DATA_DIR}/genres_acceptable.csv'
IMG_SIZE = 224  # 224 binary & pretrained, 112 cnn

TEST_FRAC = 0.2
VAL_FRAC = 0.2 * (1 - TEST_FRAC)

Path(BASE_RESULTS_DIR).mkdir(parents=True, exist_ok=True)
genres_dict = OrderedDict(sorted(read_csv(GENRES_CSV_PATH, header=None, index_col=0,
                                          squeeze=True).items(), key=lambda x: x[0]))
tfrandom = tf.random.Generator.from_seed(1337, alg='philox')
random = Random()

### import locally only #########################
num_classes = len(genres_dict)
#################################################

stoppers = [
    tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=3, restore_best_weights=True),
    tf.keras.callbacks.EarlyStopping(monitor='val_balanced_accuracy', patience=5, mode='max')
]


class Mode(Enum):
    MULTI_LABEL = 'Multilabel'
    MULTI_CLASS = 'Multiclass'


class NetworkType(Enum):
    CUSTOM = 'Custom'
    PRETRAINED = 'Pretrained'
    CNN = 'CNN'


def get_tuner(hypermodel, project_name, variant):
    if variant == 'Hyperband':
        return kt.tuners.Hyperband(
            hypermodel,
            objective='val_loss',
            max_epochs=35,
            factor=4,
            hyperband_iterations=3,
            directory=f'X:/neural/',
            project_name=project_name
        )
    elif variant == 'BayesianOptimization':
        return kt.tuners.BayesianOptimization(
            hypermodel,
            objective='val_loss',
            max_trials=200,
            num_initial_points=5,
            directory=f'X:/neural/',
            project_name=project_name
        )
    else:
        raise Exception('Wrong tuner choice')


def get_model(activation, use_dropout, dropout_rate, use_bn, bn_momentum, bn_center_scale, learning_rate,
              use_momentum, momentum, nesterov, amsgrad, initial_accumulator_value, centered, grad_averaging,
              optimizer, hidden_count, hidden_sizes, output_size, output_activation, init_bias, loss, metrics, ntype):
    if hidden_count != len(hidden_sizes):
        raise Exception('Wrong number of hidden layers')

    if optimizer == 'Adadelta':
        o = tf.keras.optimizers.Adadelta(learning_rate)
    elif optimizer == 'Adagrad':
        o = tf.keras.optimizers.Adagrad(learning_rate, initial_accumulator_value=initial_accumulator_value)
    elif optimizer == 'Adam':
        o = tf.keras.optimizers.Adam(learning_rate, amsgrad=amsgrad)
    elif optimizer == 'Adamax':
        o = tf.keras.optimizers.Adamax(learning_rate)
    elif optimizer == 'Ftrl':
        o = tf.keras.optimizers.Ftrl(learning_rate, initial_accumulator_value=initial_accumulator_value)
    elif optimizer == 'Nadam':
        o = tf.keras.optimizers.Nadam(learning_rate)
    elif optimizer == 'RMSprop':
        o = tf.keras.optimizers.RMSprop(learning_rate, momentum=momentum if use_momentum else .0, centered=centered)
    elif optimizer == 'SGD':
        o = tf.keras.optimizers.SGD(learning_rate, momentum=momentum if use_momentum else .0, nesterov=nesterov)
    elif optimizer == 'LAMB':
        o = tfa.optimizers.LAMB(learning_rate)
    elif optimizer == 'LazyAdam':
        o = tfa.optimizers.LazyAdam(learning_rate, amsgrad=amsgrad)
    elif optimizer == 'NovoGrad':
        o = tfa.optimizers.NovoGrad(learning_rate, amsgrad=amsgrad, grad_averaging=grad_averaging)
    elif optimizer == 'ProximalAdagrad':
        o = tfa.optimizers.ProximalAdagrad(learning_rate, initial_accumulator_value=initial_accumulator_value)
    elif optimizer == 'Yogi':
        o = tfa.optimizers.Yogi(learning_rate)
    else:
        raise Exception('Wrong optimizer choice')

    model = tf.keras.Sequential()
    if ntype == NetworkType.CUSTOM:
        model.add(tf.keras.layers.Flatten(input_shape=(IMG_SIZE, IMG_SIZE, 3), name='input'))
    elif ntype == NetworkType.PRETRAINED:
        model.add(KerasLayer(
            'https://tfhub.dev/google/imagenet/mobilenet_v2_100_224/feature_vector/4',
            input_shape=(IMG_SIZE, IMG_SIZE, 3),
            trainable=False,
            name='input_pretrained'
        ))
    else:
        raise Exception('Wrong network type choice')
    for i in range(len(hidden_sizes)):
        model.add(tf.keras.layers.Dense(
            hidden_sizes[i], name=f'hidden{i}',
            activation=activation, kernel_initializer='lecun_normal' if activation == 'selu' else None))
        if use_bn and i != hidden_count - 1:
            model.add(tf.keras.layers.BatchNormalization(momentum=bn_momentum,
                                                         center=bn_center_scale, scale=bn_center_scale))
        if use_dropout:
            if activation == 'selu':
                model.add(tf.keras.layers.AlphaDropout(dropout_rate))
            else:
                model.add(tf.keras.layers.Dropout(dropout_rate))
    model.add(tf.keras.layers.Dense(output_size, activation=output_activation, name='output',
                                    bias_initializer=tf.keras.initializers.Constant(init_bias)
                                    if init_bias is not None else None))

    model.compile(optimizer=o, loss=loss, metrics=metrics)

    return model


def get_model_cnn(activation, use_dropout, dropout_rate, use_bn, bn_momentum, bn_center_scale, learning_rate,
                  use_momentum, momentum, nesterov, amsgrad, initial_accumulator_value, centered, grad_averaging,
                  optimizer, hidden_count, hidden_sizes, convpool_count, convpool_sizes, conv_filter_start_size,
                  kernel_size, output_size, output_activation, init_bias, loss, metrics):
    if hidden_count != len(hidden_sizes):
        raise Exception('Wrong number of hidden layers')
    if convpool_count != len(convpool_sizes):
        raise Exception('Wrong number of convolution-pooling layers')

    if optimizer == 'Adadelta':
        o = tf.keras.optimizers.Adadelta(learning_rate)
    elif optimizer == 'Adagrad':
        o = tf.keras.optimizers.Adagrad(learning_rate, initial_accumulator_value=initial_accumulator_value)
    elif optimizer == 'Adam':
        o = tf.keras.optimizers.Adam(learning_rate, amsgrad=amsgrad)
    elif optimizer == 'Adamax':
        o = tf.keras.optimizers.Adamax(learning_rate)
    elif optimizer == 'Ftrl':
        o = tf.keras.optimizers.Ftrl(learning_rate, initial_accumulator_value=initial_accumulator_value)
    elif optimizer == 'Nadam':
        o = tf.keras.optimizers.Nadam(learning_rate)
    elif optimizer == 'RMSprop':
        o = tf.keras.optimizers.RMSprop(learning_rate, momentum=momentum if use_momentum else .0, centered=centered)
    elif optimizer == 'SGD':
        o = tf.keras.optimizers.SGD(learning_rate, momentum=momentum if use_momentum else .0, nesterov=nesterov)
    elif optimizer == 'LAMB':
        o = tfa.optimizers.LAMB(learning_rate)
    elif optimizer == 'LazyAdam':
        o = tfa.optimizers.LazyAdam(learning_rate, amsgrad=amsgrad)
    elif optimizer == 'NovoGrad':
        o = tfa.optimizers.NovoGrad(learning_rate, amsgrad=amsgrad, grad_averaging=grad_averaging)
    elif optimizer == 'ProximalAdagrad':
        o = tfa.optimizers.ProximalAdagrad(learning_rate, initial_accumulator_value=initial_accumulator_value)
    elif optimizer == 'Yogi':
        o = tfa.optimizers.Yogi(learning_rate)
    else:
        raise Exception('Wrong optimizer choice')

    model = tf.keras.Sequential()
    model.add(tf.keras.layers.Conv2D(input_shape=(IMG_SIZE, IMG_SIZE, 3),
              filters=conv_filter_start_size, kernel_size=kernel_size,
              activation='relu', padding='same', name='conv0_0'))
    for c in range(1, convpool_sizes[0]):
        model.add(tf.keras.layers.Conv2D(
            conv_filter_start_size, kernel_size, activation='relu', padding='same', name=f'conv0_{c}'
        ))
    model.add(tf.keras.layers.MaxPool2D(name='pooling0'))
    if use_bn:
        model.add(tf.keras.layers.BatchNormalization(momentum=bn_momentum,
                                                     center=bn_center_scale, scale=bn_center_scale))
    if use_dropout:
        model.add(tf.keras.layers.Dropout(dropout_rate))
    for p in range(1, convpool_count):
        for c in range(convpool_sizes[p]):
            model.add(tf.keras.layers.Conv2D(
                (2 ** p) * conv_filter_start_size, kernel_size,
                activation='relu', padding='same', name=f'conv{p}_{c}'
            ))
        model.add(tf.keras.layers.MaxPool2D(name=f'pooling{p}'))
        if use_bn:
            model.add(tf.keras.layers.BatchNormalization(momentum=bn_momentum,
                                                         center=bn_center_scale, scale=bn_center_scale))
        if use_dropout:
            model.add(tf.keras.layers.Dropout(dropout_rate))
    model.add(tf.keras.layers.Flatten(name='flatten'))
    for h in range(hidden_count):
        model.add(tf.keras.layers.Dense(
            hidden_sizes[h], name=f'hidden{h}',
            activation=activation, kernel_initializer='lecun_normal' if activation == 'selu' else None))
        if use_bn and h != hidden_count - 1:
            model.add(tf.keras.layers.BatchNormalization(momentum=bn_momentum,
                                                         center=bn_center_scale, scale=bn_center_scale))
        if use_dropout:
            if activation == 'selu':
                model.add(tf.keras.layers.AlphaDropout(dropout_rate))
            else:
                model.add(tf.keras.layers.Dropout(dropout_rate))
    model.add(tf.keras.layers.Dense(output_size, activation=output_activation, name='output',
                                    bias_initializer=tf.keras.initializers.Constant(init_bias)
                                    if init_bias is not None else None))

    model.compile(optimizer=o, loss=loss, metrics=metrics)

    return model


def encode(arr):
    s = 0
    for i, x in enumerate(arr):
        s += 2 ** i * x
    return [s]


def decode(num, bits):
    arr = []
    for i in range(bits - 1, -1, -1):
        if num - 2 ** i >= 0:
            arr.append(1)
            num -= 2 ** i
        else:
            arr.append(0)
    return np.array([arr])


def arr_to_str(arr):
    return f'[{", ".join(arr)}]'


def load_data(mode: Mode):
    ds = read_csv(MOVIES_CSV_PATH)
    ds['genres'] = ds['genres'].apply(loads)

    # genres_count = Counter()
    # ds['genres'].apply(lambda x: genres_count.update(x))
    # genres_count = OrderedDict(sorted(genres_count.items(), key=lambda x: -x[1]))

    if mode == Mode.MULTI_LABEL:
        genres_count = Counter()
        ds['genres'].apply(lambda x: genres_count.update(x))
        genres_count = np.array(list(map(lambda x: x[1], sorted(genres_count.items(), key=lambda x: x[0]))))
        genres_count_sum = np.sum(genres_count)
        init_bias = np.log(genres_count / (genres_count_sum - genres_count))

    all_x = [f'{POSTER_DIR}/{x}.jpg' for x in ds['id']]
    all_y = ds['genres'].values

    binarizer = MultiLabelBinarizer()
    binarizer.fit([list(map(lambda x: x[0], genres_dict.items()))])
    all_y = binarizer.transform(all_y)

    if mode == Mode.MULTI_CLASS:
        all_y = np.array([encode(x) for x in all_y])

        encoder = OneHotEncoder(sparse=False, dtype=np.int32)
        encoder.fit(all_y)
        all_y = encoder.transform(all_y)

        binarizer = (binarizer, encoder)

        genre_sets, genre_sets_count = np.unique(all_y, return_counts=True, axis=0)
        genre_sets_count_sum = np.sum(genre_sets_count)

        global num_classes
        num_classes = len(genre_sets)
        init_bias = np.log(genre_sets_count / (genre_sets_count_sum - genre_sets_count))

    train_x, test_x, train_y, test_y = train_test_split(
        all_x, all_y,
        test_size=TEST_FRAC,
        random_state=1337,
        stratify=all_y
    )

    train_x, val_x, train_y, val_y = train_test_split(
        train_x, train_y,
        test_size=VAL_FRAC,
        random_state=1337,
        stratify=train_y
    )

    # asd = np.unique([y for x in train_y for y in x])
    # qwe = np.unique([y for x in val_y for y in x])
    # zxc = np.unique([y for x in test_y for y in x])
    # print(asd)
    # print(qwe)
    # print(zxc)
    # asd = np.unique(train_y, axis=0)
    # qwe = np.unique(val_y, axis=0)
    # zxc = np.unique(test_y, axis=0)
    # print(asd.shape)
    # print(qwe.shape)
    # print(zxc.shape)
    # print(np.unique(all_y, axis=0).shape)
    # quit()

    return train_x, test_x, val_x, val_y, train_y, test_y, binarizer, init_bias


def load_image(path, labels):
    img = tf.io.read_file(path)
    img = tf.image.decode_jpeg(img, channels=3)
    img = tf.image.resize(img, [IMG_SIZE, IMG_SIZE])
    img = tf.cast(img, tf.float32) / 255.0
    return img, labels


def augment(img, label):
    seed = tfrandom.make_seeds(2)[0]
    img = tf.image.stateless_random_flip_up_down(img, seed)
    img = tf.image.stateless_random_flip_left_right(img, seed)
    img = tf.image.rot90(img, random.randint(0, 3))
    # img = tf.image.stateless_random_jpeg_quality(img, 20, 60, seed)
    c = int(.6 * IMG_SIZE)
    img = tf.image.stateless_random_crop(img, [c, c, 3], seed)
    img = tf.image.resize(img, [IMG_SIZE, IMG_SIZE])
    return img, label


def tf_dataset(x, y, training, augmenting=0):
    ds = tf.data.Dataset.from_tensor_slices((x, y))
    ds = ds.map(load_image, num_parallel_calls=tf.data.AUTOTUNE)

    if training:
        for _ in range(augmenting):
            augmentation = ds.map(augment, num_parallel_calls=tf.data.AUTOTUNE)
            ds = ds.concatenate(augmentation)
        ds = ds.shuffle(buffer_size=1024, seed=1337)

    ds = ds.batch(256)
    ds = ds.prefetch(buffer_size=tf.data.AUTOTUNE)

    return ds


def plot_metric(history, metric_name, test_score):
    metric = history.history[metric_name]
    val_metric = history.history[f'val_{metric_name}']

    epochs = len(metric)

    style.use('bmh')
    pyplot.figure(figsize=(8, 4))
    pyplot.plot(range(1, epochs + 1), metric, label='training')
    pyplot.plot(range(1, epochs + 1), val_metric, label='validation')
    pyplot.legend()
    pyplot.ylabel(metric_name)
    pyplot.title(f'Training and Validation {metric_name}')
    pyplot.xlabel('epoch\n\n'
                  f'Test score: {test_score:.4f}')

    pyplot.tight_layout()
    pyplot.savefig(f'{BASE_RESULTS_DIR}/{metric_name}')
    pyplot.close()

    df = DataFrame(np.array([metric, val_metric]).T, columns=['training', 'validation'])
    df.to_csv(f'{BASE_RESULTS_DIR}/{metric_name}.csv', index=False)

    return metric, val_metric


def plot_roc(y_pred, target):
    fpr, tpr, _ = roc_curve(target.ravel(), y_pred.ravel())

    auc_ = auc(fpr, tpr)

    pyplot.figure(figsize=(8, 8))
    pyplot.plot(fpr, tpr)
    pyplot.plot([0, 1], [0, 1], 'k:', alpha=.2)
    pyplot.ylabel('TP rate')
    pyplot.title('ROC curve')
    pyplot.xlabel('FP rate\n\n'
                  f'AUC = {auc_}')

    pyplot.xlim(-.05, 1.05)
    pyplot.ylim(-.05, 1.05)

    pyplot.tight_layout()
    pyplot.savefig(f'{BASE_RESULTS_DIR}/roc_curve')
    pyplot.close()

    df = DataFrame(np.array([fpr, tpr]).T, columns=['fpr', 'tpr'])
    df.to_csv(f'{BASE_RESULTS_DIR}/roc.csv', index=False)

    return auc_


def plot_pr(y_pred, target):
    p, r, _ = precision_recall_curve(target.ravel(), y_pred.ravel())

    auc_ = average_precision_score(target.ravel(), y_pred.ravel())

    pyplot.figure(figsize=(8, 8))
    pyplot.plot(r, p)
    pyplot.ylabel('precision')
    pyplot.title('PR curve')
    pyplot.xlabel('recall\n\n'
                  f'AUC = {auc_}\n')

    pyplot.xlim(-.05, 1.05)
    pyplot.ylim(-.05, 1.05)

    pyplot.tight_layout()
    pyplot.savefig(f'{BASE_RESULTS_DIR}/pr_curve')
    pyplot.close()

    df = DataFrame(np.array([p, r]).T, columns=['p', 'r'])
    df.to_csv(f'{BASE_RESULTS_DIR}/pr.csv', index=False)

    return auc_


def plot_prediction_bias(y_pred, target, label_names, threshold=0.5):
    y_pred = tf.cast(tf.greater(y_pred, threshold), tf.float32)
    y_true = tf.cast(target, tf.float32)

    y_pred_avg = np.mean(y_pred, axis=0)
    y_true_avg = np.mean(y_true, axis=0)

    bias = y_pred_avg / y_true_avg * 100 - 100

    style.use('bmh')
    pyplot.figure(figsize=(8, 8))
    yticks = range(len(label_names))
    width = .24
    pyplot.barh([x + .5 * width for x in yticks], y_true_avg, width, label='true')
    pyplot.barh([x - .5 * width for x in yticks], y_pred_avg, width, label='prediction')
    pyplot.ylabel('label')
    pyplot.title(f'Prediction Bias')
    pyplot.yticks(yticks, label_names)

    offset = pyplot.xticks()[0]
    offset = .025 * (offset[1] - offset[0])
    for i, (b, x) in enumerate(zip(bias, [max(p, t) for p, t in zip(y_pred_avg, y_true_avg)])):
        pyplot.text(x + offset, yticks[i] - .75 * width, f'{b:+.1f}%', fontsize='small')

    bias_avg = tf.math.abs(bias)
    bias_avg = tf.reduce_mean(bias_avg)
    pyplot.xlabel('Average occurrence\n\n'
                  f'Average bias: {bias_avg:.1f}%')

    pyplot.tight_layout()
    pyplot.legend()
    pyplot.savefig(f'{BASE_RESULTS_DIR}/prediction_bias')
    pyplot.close()

    df = DataFrame(np.array([label_names, y_true_avg, y_pred_avg]).T,
                   columns=['label_names', 'y_true_avg', 'y_pred_avg'])
    df.to_csv(f'{BASE_RESULTS_DIR}/prediction_bias.csv', index=False)


def plot_confusion_matrix(y_pred, target, label_names, threshold=0.5):
    y_pred = tf.cast(tf.greater(y_pred, threshold), tf.float32)

    cm = tfa.metrics.MultiLabelConfusionMatrix(num_classes)
    cm.update_state(target, y_pred)
    cm = tf.cast(cm.result(), tf.int32).numpy()

    subplot_h = floor(sqrt(len(label_names)))
    subplot_w = ceil(len(label_names) / subplot_h)

    style.use('bmh')
    pyplot.figure(figsize=(4 * subplot_w, 3 * subplot_h))

    for i, label in enumerate(label_names):
        pyplot.subplot(subplot_h, subplot_w, i + 1)
        seaborn.heatmap(cm[i], annot=True, fmt='d', cbar=False)
        if (i + 1) % subplot_w == 1:
            pyplot.ylabel('Target')
        pyplot.title(label)
        if (i + 1) > subplot_w * subplot_h - subplot_w:
            pyplot.xlabel('Prediction')
    pyplot.suptitle('Confusion Matrices')

    pyplot.tight_layout()
    pyplot.savefig(f'{BASE_RESULTS_DIR}/confusion_matrix')
    pyplot.close()

    cm = np.array([x.reshape(4) for x in cm])
    df = DataFrame(np.array([label_names, cm[:, 0], cm[:, 1], cm[:, 2], cm[:, 3]]).T,
                   columns=['label_names', 'TN', 'FP', 'FN', 'TP'])
    df.to_csv(f'{BASE_RESULTS_DIR}/confusion_matrix.csv', index=False)


def save_results(metrics, history, model, test_ds, test_y, labels):
    missing = []
    test_eval = model.evaluate(test_ds)
    i = 0
    for metric in metrics:
        try:
            plot_metric(history, metric, test_eval[i])
            i += 1
        except KeyError:
            missing.append(metric)
    df = DataFrame(np.array([[x for x in metrics if x not in missing], test_eval]).T, columns=['metric', 'score'])

    y_pred = model.predict(test_ds)
    try:
        auc_roc = plot_roc(y_pred, test_y)
        df = df.append({'metric': 'auc_roc', 'score': auc_roc}, ignore_index=True)
    except ValueError:
        missing.append('roc')
    try:
        auc_pr = plot_pr(y_pred, test_y)
        df = df.append({'metric': 'auc_pr', 'score': auc_pr}, ignore_index=True)
    except ValueError:
        missing.append('pr')
    try:
        plot_prediction_bias(y_pred, test_y, labels)
    except ValueError:
        missing.append('prediction_bias')
    plot_confusion_matrix(y_pred, test_y, labels)

    if missing:
        print('\nSome metrics could not be handled:', missing)
    df.to_csv(f'{BASE_RESULTS_DIR}/test_eval.csv', index=False)
    model.save(f'{BASE_RESULTS_DIR}/model')
