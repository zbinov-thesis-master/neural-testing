from math import floor, sqrt, ceil
from pathlib import Path

import numpy as np
import seaborn
from pandas import read_csv, DataFrame
from matplotlib import pyplot, style

from common import arr_to_str

COLORS = ['b', 'r', 'c', 'm', 'y', 'g']
BASE_RESULTS_DIR = f'P:/meng/custom_binary/double_f1'
OUTPUT_RESULTS_DIR = f'{BASE_RESULTS_DIR}/_joined'
Path(OUTPUT_RESULTS_DIR).mkdir(parents=True, exist_ok=True)

TRANSLATE = True
DIRS = ['001',
        '002',
        '003',
        '004',
        '005']
SIZE = 6

translation = {
    'training': 'trening',
    'validation': 'walidacja',
    'epoch': 'epoka',
    'target': 'cel',
    'true': 'rzeczywista',
    'prediction': 'predykcja',
    'label': 'etykieta',
    'average occurrence': 'średnia częstotliwość występowania',
    'TP rate': 'prawdziwie pozytywna stopa',
    'FP rate': 'fałszywie pozytywna stopa',
    'accuracy': 'dokładność',
    'aucpr': 'AUC PR',
    'aucroc': 'AUC ROC',
    'auc_pr': 'AUC PR',
    'auc_roc': 'AUC ROC',
    'balanced_accuracy': 'zbalansowana dokładność',
    'cohen_kappa': 'kappa Cohena',
    'contrastive_loss': 'ranking loss', #
    'double_precision': 'dwustronna precyzja',
    'double_recall': 'dwustronna czułość',
    'f1_score_macro': 'f1 makrouśredniane',
    'f1_score_micro': 'f1 mikrouśredniane',
    'false_discovery_rate': 'współczynnik fałszywego wykrycia',
    'false_negative_rate': 'współczynnik wyników fałszywie negatywnych',
    'false_omission_rate': 'współczynnik fałszywego pominięcia',
    'false_positive_rate': 'współczynnik wyników fałszywie pozytywnych',
    'fbeta_score_macro': 'fβ makrouśredniane',
    'fbeta_score_micro': 'fβ mikrouśredniane',
    'fowlkes_mallows_index': 'indeks Fowlkes-Mallows',
    'hamming_loss': 'Hamming loss',
    'informedness': 'informedness', #
    'loss': 'strata',
    'macro_double_f1': 'dwustronne f1 makrouśredniane',
    'macro_f1': 'f1 makrouśredniane',
    'markedness': 'markedness', #
    'matthews_correlation': 'współczynnik korelacji Matthew',
    'negative_predictive_value': 'negatywna wartość prognostyczna',
    'positive_predictive_value': 'pozytywna wartość prognostyczna',
    'threat_score': 'threat_score', #
    'true_negative_rate': 'współczynnik wyników prawdziwie negatywnych',
    'true_positive_rate': 'współczynnik wyników prawdziwie pozytywnych',
    'precision': 'precyzja',
    'recall': 'czułość'
}


def jagged_to_nan(data):
    biggest_size = np.max([d.shape[0] for d in data])
    for i in range(len(data)):
        for _ in range(biggest_size - data[i].shape[0]):
            data[i] = np.vstack((data[i], [np.NaN, np.NaN]))
    return np.array(data)


def adjust_distribution(x_, y_):
    shortest_x = np.min([np.count_nonzero(~np.isnan(x)) for x in x_])
    shortest_idx = np.argmin([np.count_nonzero(~np.isnan(x)) for x in x_])
    x_nonan = x_[shortest_idx][~np.isnan(x_[shortest_idx])]
    ny = []
    for i in range(len(y_)):
        if i == shortest_idx:
            ny.append(y_[i][~np.isnan(y_[i])])
        else:
            ny.append(np.resize(y_[i][~np.isnan(y_[i])], shortest_x))
    ym = np.mean(ny, axis=0)
    ys = np.std(ny, axis=0)
    return np.array([x_nonan, ym, ys])


def load_data(paths, to_nan=True):
    data = []
    for i, path in enumerate(paths):
        data.append(read_csv(path,
                             names=[f'{i}_0', f'{i}_1'], skiprows=1,
                             index_col=None).values)
    if to_nan:
        data = jagged_to_nan(data)
    return data


def plot_metric(data, metric_name):
    style.use('bmh')
    pyplot.figure(figsize=(SIZE, SIZE))

    tm = np.nanmean(data[:, :, 0], axis=0)
    ts = np.nanstd(data[:, :, 0], axis=0)
    vm = np.nanmean(data[:, :, 1], axis=0)
    vs = np.nanstd(data[:, :, 1], axis=0)
    pyplot.plot(range(1, len(tm) + 1), tm, alpha=.8, label='training' if not TRANSLATE else 'trening')
    pyplot.fill_between(range(1, len(ts) + 1), tm - ts, tm + ts, alpha=.2)
    pyplot.plot(range(1, len(vm) + 1), vm, alpha=.8, label='validation' if not TRANSLATE else 'walidacja')
    pyplot.fill_between(range(1, len(vs) + 1), vm - vs, vm + vs, alpha=.2)

    pyplot.ylim(-.05, 1.05)

    pyplot.legend()
    pyplot.ylabel(metric_name if not TRANSLATE else 'wartość')
    pyplot.xlabel('epoch' if not TRANSLATE else 'epoka')

    pyplot.tight_layout()
    pyplot.savefig(f'{OUTPUT_RESULTS_DIR}/{metric_name}')
    pyplot.close()

    df = DataFrame()
    df['train_mean'] = tm
    df['train_std'] = ts
    df['val_mean'] = vm
    df['val_std'] = vs
    df.to_csv(f'{OUTPUT_RESULTS_DIR}/{metric_name}.csv', index=False)


def plot_roc(data):
    style.use('bmh')
    pyplot.figure(figsize=(SIZE, SIZE))

    pyplot.plot([0, 1], [0, 1], 'k:', alpha=.2)

    xm = np.nanmean(data[:, :, 0], axis=0)
    ym = np.nanmean(data[:, :, 1], axis=0)
    ys = np.nanstd(data[:, :, 1], axis=0)
    pyplot.plot(xm, ym, alpha=.8)
    # pyplot.fill_between(xm, ym - ys, ym + ys, alpha=.2)

    # xy = adjust_distribution(data[:, :, 0], data[:, :, 1])
    # pyplot.plot(xy[0], xy[1], alpha=.8)
    # pyplot.fill_between(xy[0], xy[1] - xy[2], xy[1] + xy[2], alpha=.2)

    pyplot.xlim(-.05, 1.05)
    pyplot.ylim(-.05, 1.05)

    pyplot.ylabel('TP rate' if not TRANSLATE else translation['TP rate'])
    pyplot.xlabel('FP rate' if not TRANSLATE else translation['FP rate'])

    pyplot.tight_layout()
    pyplot.savefig(f'{OUTPUT_RESULTS_DIR}/roc')
    pyplot.close()

    df = DataFrame()
    df['fpr_mean'] = xm
    df['tpr_mean'] = ym
    df['tpr_std'] = ys
    df.to_csv(f'{OUTPUT_RESULTS_DIR}/roc.csv', index=False)


def plot_pr(data):
    style.use('bmh')
    pyplot.figure(figsize=(SIZE, SIZE))

    xy = adjust_distribution(data[:, :, 1], data[:, :, 0])
    pyplot.plot(xy[0], xy[1], alpha=.8)
    pyplot.fill_between(xy[0], xy[1] - xy[2], xy[1] + xy[2], alpha=.2)

    pyplot.xlim(-.05, 1.05)
    pyplot.ylim(-.05, 1.05)

    pyplot.ylabel('precision' if not TRANSLATE else translation['precision'])
    pyplot.xlabel('recall' if not TRANSLATE else translation['recall'])

    pyplot.tight_layout()
    pyplot.savefig(f'{OUTPUT_RESULTS_DIR}/pr')
    pyplot.close()

    df = DataFrame()
    df['p_mean'] = xy[1]
    df['r_mean'] = xy[0]
    df['p_std'] = xy[2]
    df.to_csv(f'{OUTPUT_RESULTS_DIR}/pr.csv', index=False)


def plot_prediction_bias(paths):
    label_names = read_csv(paths[0], usecols=['label_names']).values
    data = load_data(paths)
    data = data.mean(axis=0)
    bias = data[:, 1] / data[:, 0] * 100 - 100

    style.use('bmh')
    pyplot.figure(figsize=(SIZE, SIZE))
    yticks = range(len(label_names))
    width = .24

    pyplot.barh([x + .5 * width for x in yticks], data[:, 0], width,
                label='true' if not TRANSLATE else translation['true'])
    pyplot.barh([x - .5 * width for x in yticks], data[:, 1], width,
                label='prediction' if not TRANSLATE else translation['prediction'])

    offset = pyplot.xticks()[0]
    offset = .025 * (offset[1] - offset[0])
    for i, (b, x) in enumerate(zip(bias, [max(p, t) for p, t in zip(data[:, 1], data[:, 0])])):
        pyplot.text(x + offset, yticks[i] - .75 * width, f'{b:+.1f}%', fontsize='small')

    pyplot.ylabel('label' if not TRANSLATE else translation['label'])
    pyplot.yticks(yticks, [arr_to_str(x)[1:-1] for x in label_names])
    pyplot.xlabel('average occurrence' if not TRANSLATE else translation['average occurrence'])

    pyplot.legend(bbox_to_anchor=(1, .97), loc='lower right', ncol=2)
    pyplot.tight_layout()
    pyplot.savefig(f'{OUTPUT_RESULTS_DIR}/prediction_bias')
    pyplot.close()

    df = DataFrame(np.append(label_names, data, axis=1), columns=['label_names', 'y_true_avg', 'y_pred_avg'])
    df.to_csv(f'{OUTPUT_RESULTS_DIR}/prediction_bias.csv', index=False)


def plot_confusion_matrix(paths):
    label_names = read_csv(paths[0], usecols=['label_names']).values
    data = np.array([read_csv(p, usecols=['TN', 'FP', 'FN', 'TP'], index_col=None).values for p in paths])

    data = data.mean(axis=0).astype(np.int32)
    data = np.array([x.reshape((2, 2)) for x in data])

    subplot_h = floor(sqrt(len(label_names)))
    subplot_w = ceil(len(label_names) / subplot_h)

    style.use('bmh')
    pyplot.figure(figsize=(3 * subplot_w, 2.25 * subplot_h))

    for i, label in enumerate([arr_to_str(x)[1:-1] for x in label_names]):
        pyplot.subplot(subplot_h, subplot_w, i + 1)
        pyplot.xticks(fontsize=13)
        pyplot.yticks(fontsize=13)
        seaborn.heatmap(data[i], annot=True, fmt='d', cbar=False, annot_kws={'size': 20})
        if (i + 1) % subplot_w == 1:
            pyplot.ylabel('target' if not TRANSLATE else translation['target'])
        pyplot.title(label)
        if (i + 1) > subplot_w * subplot_h - subplot_w:
            pyplot.xlabel('prediction' if not TRANSLATE else translation['prediction'])

    pyplot.tight_layout()
    pyplot.savefig(f'{OUTPUT_RESULTS_DIR}/confusion_matrix')
    pyplot.close()

    data = np.array([x.reshape(4) for x in data])
    df = DataFrame(np.append(label_names, data, axis=1),
                   columns=['label_names', 'TN', 'FP', 'FN', 'TP'])
    df.to_csv(f'{OUTPUT_RESULTS_DIR}/confusion_matrix.csv', index=False)


def test_eval_merge(paths):
    data = read_csv(paths[0], usecols=['metric'])
    for i, path in enumerate(paths):
        data[f'{i + 1:03}'] = read_csv(path, usecols=['score'])
    data['score'] = data.mean(axis=1)
    data['std'] = data.std(axis=1)
    data = data[['metric', 'score', 'std']]
    data.to_csv(f'{OUTPUT_RESULTS_DIR}/test_eval.csv', index=False, float_format='%.3f')


def main():
    missing = []
    for metric in ['loss', 'macro_double_f1', 'accuracy', 'balanced_accuracy', 'double_precision', 'double_recall',
                   'aucroc', 'aucpr', 'cohen_kappa', 'f1_score_macro', 'f1_score_micro',
                   'fbeta_score_macro', 'fbeta_score_micro', 'hamming_loss', 'contrastive_loss',
                   'true_positive_rate', 'true_negative_rate', 'positive_predictive_value', 'negative_predictive_value',
                   'false_negative_rate', 'false_positive_rate', 'false_discovery_rate', 'false_omission_rate',
                   'threat_score', 'fowlkes_mallows_index', 'matthews_correlation', 'informedness', 'markedness',
                   'macro_f1']:
        try:
            data = load_data([f'{BASE_RESULTS_DIR}/{x}/{metric}.csv' for x in DIRS])
            plot_metric(data, metric)
            print(metric)
        except FileNotFoundError:
            missing.append(metric)

    try:
        data = load_data([f'{BASE_RESULTS_DIR}/{x}/roc.csv' for x in DIRS])
        plot_roc(data)
    except FileNotFoundError:
        missing.append('roc')

    try:
        data = load_data([f'{BASE_RESULTS_DIR}/{x}/pr.csv' for x in DIRS])
        plot_pr(data)
    except FileNotFoundError:
        missing.append('pr')

    try:
        plot_prediction_bias([f'{BASE_RESULTS_DIR}/{x}/prediction_bias.csv' for x in DIRS])
    except FileNotFoundError:
        missing.append('prediction_bias')

    try:
        plot_confusion_matrix([f'{BASE_RESULTS_DIR}/{x}/confusion_matrix.csv' for x in DIRS])
    except FileNotFoundError:
        missing.append('confusion_matrix')

    try:
        test_eval_merge([f'{BASE_RESULTS_DIR}/{x}/test_eval.csv' for x in DIRS])
    except FileNotFoundError:
        missing.append('test_eval')

    if missing:
        print('\nSome items could not be found:', missing)


main()
