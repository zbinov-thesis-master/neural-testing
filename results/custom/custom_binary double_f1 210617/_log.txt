"C:\Program Files\Python37\python.exe" main_binary.py
Results summary
Results in X:/neural/CUSTOM-BIN
Showing 10 best trials
Objective(name='val_loss', direction='min')
Trial summary
Hyperparameters:
activation: selu
use_dropout: False
dropout_rate: 0.7000000000000002
use_bn: True
bn_momentum: 0.8000000000000003
bn_center_scale: False
learning_rate: 0.040194380063875615
use_momentum: False
momentum: 0.956025439382759
nesterov: False
amsgrad: True
initial_accumulator_value: 0.01
centered: True
grad_averaging: False
optimizer: NovoGrad
hidden_count: 4
units0: 512
units1: 768
units2: 432
units3: 496
units4: 288
units5: 624
units6: 352
units7: 608
units8: 336
units9: 80
units10: 528
units11: 128
units12: 624
units13: 128
units14: 48
tuner/epochs: 9
tuner/initial_epoch: 3
tuner/bracket: 2
tuner/round: 1
tuner/trial_id: 283b485a9c2ee2c39961016a28cb6193
Score: 0.4072118401527405
Trial summary
Hyperparameters:
activation: relu
use_dropout: False
dropout_rate: 0.5000000000000001
use_bn: True
bn_momentum: 0.7500000000000002
bn_center_scale: False
learning_rate: 0.008597648530160746
use_momentum: True
momentum: 0.9782939955519879
nesterov: False
amsgrad: True
initial_accumulator_value: 0.01
centered: True
grad_averaging: True
optimizer: Yogi
hidden_count: 3
units0: 112
units1: 336
units2: 720
units3: 448
units4: 528
units5: 112
units6: 480
units7: 640
units8: 224
units9: 272
units10: 368
units11: 496
units12: 288
units13: 64
units14: 480
tuner/epochs: 9
tuner/initial_epoch: 0
tuner/bracket: 1
tuner/round: 0
Score: 0.4092631936073303
Trial summary
Hyperparameters:
activation: selu
use_dropout: False
dropout_rate: 0.5000000000000001
use_bn: True
bn_momentum: 0.7500000000000002
bn_center_scale: True
learning_rate: 0.026240546978357496
use_momentum: False
momentum: 0.9699667362183229
nesterov: True
amsgrad: False
initial_accumulator_value: 0.1
centered: False
grad_averaging: True
optimizer: NovoGrad
hidden_count: 11
units0: 368
units1: 416
units2: 656
units3: 336
units4: 368
units5: 480
units6: 480
units7: 48
units8: 144
units9: 208
units10: 112
units11: 560
units12: 688
units13: 640
units14: 672
tuner/epochs: 35
tuner/initial_epoch: 9
tuner/bracket: 1
tuner/round: 1
tuner/trial_id: a74509d92acf4d04ab02d45c1e0ab115
Score: 0.41055828332901
Trial summary
Hyperparameters:
activation: gelu
use_dropout: False
dropout_rate: 0.6000000000000001
use_bn: True
bn_momentum: 0.6500000000000001
bn_center_scale: False
learning_rate: 0.007476536756587134
use_momentum: True
momentum: 0.9020782007042479
nesterov: False
amsgrad: False
initial_accumulator_value: 0.1
centered: True
grad_averaging: True
optimizer: NovoGrad
hidden_count: 3
units0: 464
units1: 640
units2: 336
units3: 288
units4: 656
units5: 704
units6: 480
units7: 624
units8: 304
units9: 112
units10: 272
units11: 256
units12: 192
units13: 432
tuner/epochs: 9
tuner/initial_epoch: 3
tuner/bracket: 2
tuner/round: 1
tuner/trial_id: 012b8a58b09783c73bc78ddf22d08ad6
Score: 0.41160306334495544
Trial summary
Hyperparameters:
activation: relu
use_dropout: False
dropout_rate: 0.30000000000000004
use_bn: True
bn_momentum: 0.8500000000000003
bn_center_scale: False
learning_rate: 0.04625487003626263
use_momentum: False
momentum: 0.9373248725841999
nesterov: True
amsgrad: False
initial_accumulator_value: 0.01
centered: False
grad_averaging: False
optimizer: Adagrad
hidden_count: 13
units0: 720
units1: 144
units2: 112
units3: 720
units4: 208
units5: 368
units6: 224
units7: 160
units8: 480
units9: 192
units10: 128
units11: 400
units12: 656
units13: 272
units14: 256
tuner/epochs: 35
tuner/initial_epoch: 9
tuner/bracket: 1
tuner/round: 1
tuner/trial_id: 134b3d39c75ffaea0f2655283d77d17a
Score: 0.41171279549598694
Trial summary
Hyperparameters:
activation: selu
use_dropout: False
dropout_rate: 0.5000000000000001
use_bn: True
bn_momentum: 0.5
bn_center_scale: False
learning_rate: 0.03802427868601088
use_momentum: True
momentum: 0.9247406547262286
nesterov: True
amsgrad: True
initial_accumulator_value: 0.1
centered: False
grad_averaging: True
optimizer: SGD
hidden_count: 8
units0: 656
units1: 240
units2: 592
units3: 704
units4: 608
units5: 544
units6: 272
units7: 352
units8: 480
units9: 400
units10: 544
units11: 352
units12: 304
units13: 624
units14: 656
tuner/epochs: 3
tuner/initial_epoch: 0
tuner/bracket: 2
tuner/round: 0
Score: 0.4126117527484894
Trial summary
Hyperparameters:
activation: gelu
use_dropout: False
dropout_rate: 0.6000000000000001
use_bn: True
bn_momentum: 0.6500000000000001
bn_center_scale: False
learning_rate: 0.007476536756587134
use_momentum: True
momentum: 0.9020782007042479
nesterov: False
amsgrad: False
initial_accumulator_value: 0.1
centered: True
grad_averaging: True
optimizer: NovoGrad
hidden_count: 3
units0: 464
units1: 640
units2: 336
units3: 288
units4: 656
units5: 704
units6: 480
units7: 624
units8: 304
units9: 112
units10: 272
units11: 256
units12: 192
units13: 432
tuner/epochs: 35
tuner/initial_epoch: 9
tuner/bracket: 2
tuner/round: 2
tuner/trial_id: 3cead70d5c350ab0f27b9dcbdcbdc7d7
Score: 0.412860631942749
Trial summary
Hyperparameters:
activation: relu
use_dropout: False
dropout_rate: 0.5000000000000001
use_bn: True
bn_momentum: 0.7500000000000002
bn_center_scale: False
learning_rate: 0.008597648530160746
use_momentum: True
momentum: 0.9782939955519879
nesterov: False
amsgrad: True
initial_accumulator_value: 0.01
centered: True
grad_averaging: True
optimizer: Yogi
hidden_count: 3
units0: 112
units1: 336
units2: 720
units3: 448
units4: 528
units5: 112
units6: 480
units7: 640
units8: 224
units9: 272
units10: 368
units11: 496
units12: 288
units13: 64
units14: 480
tuner/epochs: 35
tuner/initial_epoch: 9
tuner/bracket: 1
tuner/round: 1
tuner/trial_id: 0317ebc63feeee55f9fea5410197206d
Score: 0.4129606783390045
Trial summary
Hyperparameters:
activation: relu
use_dropout: False
dropout_rate: 0.30000000000000004
use_bn: True
bn_momentum: 0.8500000000000003
bn_center_scale: False
learning_rate: 0.04625487003626263
use_momentum: False
momentum: 0.9373248725841999
nesterov: True
amsgrad: False
initial_accumulator_value: 0.01
centered: False
grad_averaging: False
optimizer: Adagrad
hidden_count: 13
units0: 720
units1: 144
units2: 112
units3: 720
units4: 208
units5: 368
units6: 224
units7: 160
units8: 480
units9: 192
units10: 128
units11: 400
units12: 656
units13: 272
units14: 256
tuner/epochs: 9
tuner/initial_epoch: 0
tuner/bracket: 1
tuner/round: 0
Score: 0.41307970881462097
Trial summary
Hyperparameters:
activation: gelu
use_dropout: False
dropout_rate: 0.4000000000000001
use_bn: True
bn_momentum: 0.8500000000000003
bn_center_scale: False
learning_rate: 0.0024335728505646696
use_momentum: True
momentum: 0.9814432976485306
nesterov: True
amsgrad: False
initial_accumulator_value: 0.1
centered: False
grad_averaging: False
optimizer: Yogi
hidden_count: 12
units0: 576
units1: 128
units2: 80
units3: 688
units4: 112
units5: 48
units6: 720
units7: 144
units8: 256
units9: 496
units10: 688
units11: 496
units12: 240
units13: 160
units14: 480
tuner/epochs: 35
tuner/initial_epoch: 9
tuner/bracket: 2
tuner/round: 2
tuner/trial_id: adf96c5b67c1101cd6af38f993ac8316
Score: 0.41358131170272827
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
input (Flatten)              (None, 150528)            0         
_________________________________________________________________
hidden0 (Dense)              (None, 512)               77070848  
_________________________________________________________________
batch_normalization (BatchNo (None, 512)               1024      
_________________________________________________________________
hidden1 (Dense)              (None, 768)               393984    
_________________________________________________________________
batch_normalization_1 (Batch (None, 768)               1536      
_________________________________________________________________
hidden2 (Dense)              (None, 432)               332208    
_________________________________________________________________
batch_normalization_2 (Batch (None, 432)               864       
_________________________________________________________________
hidden3 (Dense)              (None, 496)               214768    
_________________________________________________________________
output (Dense)               (None, 8)                 3976      
=================================================================
Total params: 78,019,208
Trainable params: 78,015,784
Non-trainable params: 3,424
_________________________________________________________________
24/24 [==============================] - 8s 164ms/step - loss: 0.5130 - macro_double_f1: 0.4616 - accuracy: 0.7854 - balanced_accuracy: 0.4894 - double_precision: 0.4615 - double_recall: 0.4894 - aucroc: 0.4661 - aucpr: 0.1533 - cohen_kappa: -0.0362 - f1_score_macro: 0.0602 - f1_score_micro: 0.1339 - fbeta_score_macro: 0.0605 - fbeta_score_micro: 0.1633 - hamming_loss: 0.2355 - true_positive_rate: 0.0700 - true_negative_rate: 0.9088 - positive_predictive_value: 0.0914 - negative_predictive_value: 0.8316 - false_negative_rate: 0.9300 - false_positive_rate: 0.0912 - false_discovery_rate: 0.3010 - false_omission_rate: 0.1684 - threat_score: 0.0339 - fowlkes_mallows_index: 0.0661 - matthews_correlation: -0.0149 - informedness: -0.0212 - markedness: -0.0770 - macro_f1: 0.0597
Epoch 1/30
78/78 [==============================] - 24s 265ms/step - loss: 0.4280 - macro_double_f1: 0.5738 - accuracy: 0.7604 - balanced_accuracy: 0.5866 - double_precision: 0.5750 - double_recall: 0.5866 - aucroc: 0.6197 - aucpr: 0.2446 - cohen_kappa: 0.1355 - f1_score_macro: 0.3036 - f1_score_micro: 0.3331 - fbeta_score_macro: 0.2857 - fbeta_score_micro: 0.3138 - hamming_loss: 0.2027 - true_positive_rate: 0.3415 - true_negative_rate: 0.8317 - positive_predictive_value: 0.2794 - negative_predictive_value: 0.8705 - false_negative_rate: 0.6585 - false_positive_rate: 0.1683 - false_discovery_rate: 0.7190 - false_omission_rate: 0.1295 - threat_score: 0.1811 - fowlkes_mallows_index: 0.3041 - matthews_correlation: 0.1595 - informedness: 0.1731 - markedness: 0.1499 - macro_f1: 0.2982 - val_loss: 0.4141 - val_macro_double_f1: 0.5861 - val_accuracy: 0.7621 - val_balanced_accuracy: 0.6007 - val_double_precision: 0.5843 - val_double_recall: 0.6007 - val_aucroc: 0.6278 - val_aucpr: 0.2559 - val_cohen_kappa: 0.1516 - val_f1_score_macro: 0.3243 - val_f1_score_micro: 0.3487 - val_fbeta_score_macro: 0.3049 - val_fbeta_score_micro: 0.3257 - val_hamming_loss: 0.1992 - val_true_positive_rate: 0.3717 - val_true_negative_rate: 0.8297 - val_positive_predictive_value: 0.2937 - val_negative_predictive_value: 0.8748 - val_false_negative_rate: 0.6283 - val_false_positive_rate: 0.1703 - val_false_discovery_rate: 0.7063 - val_false_omission_rate: 0.1252 - val_threat_score: 0.1976 - val_fowlkes_mallows_index: 0.3270 - val_matthews_correlation: 0.1830 - val_informedness: 0.2014 - val_markedness: 0.1685 - val_macro_f1: 0.3215
Epoch 2/30
78/78 [==============================] - 21s 257ms/step - loss: 0.4056 - macro_double_f1: 0.5952 - accuracy: 0.7798 - balanced_accuracy: 0.6018 - double_precision: 0.5950 - double_recall: 0.6018 - aucroc: 0.6277 - aucpr: 0.2617 - cohen_kappa: 0.1626 - f1_score_macro: 0.3312 - f1_score_micro: 0.3591 - fbeta_score_macro: 0.3201 - fbeta_score_micro: 0.3463 - hamming_loss: 0.1950 - true_positive_rate: 0.3512 - true_negative_rate: 0.8523 - positive_predictive_value: 0.3152 - negative_predictive_value: 0.8748 - false_negative_rate: 0.6488 - false_positive_rate: 0.1477 - false_discovery_rate: 0.6848 - false_omission_rate: 0.1252 - threat_score: 0.2014 - fowlkes_mallows_index: 0.3304 - matthews_correlation: 0.1957 - informedness: 0.2036 - markedness: 0.1900 - macro_f1: 0.3276 - val_loss: 0.4193 - val_macro_double_f1: 0.5799 - val_accuracy: 0.7875 - val_balanced_accuracy: 0.5849 - val_double_precision: 0.5863 - val_double_recall: 0.5849 - val_aucroc: 0.6100 - val_aucpr: 0.2511 - val_cohen_kappa: 0.1676 - val_f1_score_macro: 0.2964 - val_f1_score_micro: 0.3457 - val_fbeta_score_macro: 0.2989 - val_fbeta_score_micro: 0.3441 - val_hamming_loss: 0.1941 - val_true_positive_rate: 0.3032 - val_true_negative_rate: 0.8665 - val_positive_predictive_value: 0.3017 - val_negative_predictive_value: 0.8709 - val_false_negative_rate: 0.6968 - val_false_positive_rate: 0.1335 - val_false_discovery_rate: 0.6983 - val_false_omission_rate: 0.1291 - val_threat_score: 0.1797 - val_fowlkes_mallows_index: 0.2973 - val_matthews_correlation: 0.1695 - val_informedness: 0.1698 - val_markedness: 0.1726 - val_macro_f1: 0.2925
Epoch 3/30
78/78 [==============================] - 21s 259ms/step - loss: 0.3955 - macro_double_f1: 0.6055 - accuracy: 0.7873 - balanced_accuracy: 0.6112 - double_precision: 0.6068 - double_recall: 0.6112 - aucroc: 0.6333 - aucpr: 0.2705 - cohen_kappa: 0.1734 - f1_score_macro: 0.3456 - f1_score_micro: 0.3722 - fbeta_score_macro: 0.3377 - fbeta_score_micro: 0.3617 - hamming_loss: 0.1930 - true_positive_rate: 0.3628 - true_negative_rate: 0.8597 - positive_predictive_value: 0.3362 - negative_predictive_value: 0.8773 - false_negative_rate: 0.6372 - false_positive_rate: 0.1403 - false_discovery_rate: 0.6638 - false_omission_rate: 0.1227 - threat_score: 0.2124 - fowlkes_mallows_index: 0.3463 - matthews_correlation: 0.2167 - informedness: 0.2225 - markedness: 0.2135 - macro_f1: 0.3432 - val_loss: 0.4139 - val_macro_double_f1: 0.5865 - val_accuracy: 0.7730 - val_balanced_accuracy: 0.5969 - val_double_precision: 0.5843 - val_double_recall: 0.5969 - val_aucroc: 0.6190 - val_aucpr: 0.2537 - val_cohen_kappa: 0.1615 - val_f1_score_macro: 0.3185 - val_f1_score_micro: 0.3529 - val_fbeta_score_macro: 0.3041 - val_fbeta_score_micro: 0.3364 - val_hamming_loss: 0.1968 - val_true_positive_rate: 0.3501 - val_true_negative_rate: 0.8437 - val_positive_predictive_value: 0.2941 - val_negative_predictive_value: 0.8746 - val_false_negative_rate: 0.6499 - val_false_positive_rate: 0.1563 - val_false_discovery_rate: 0.7059 - val_false_omission_rate: 0.1254 - val_threat_score: 0.1941 - val_fowlkes_mallows_index: 0.3185 - val_matthews_correlation: 0.1800 - val_informedness: 0.1938 - val_markedness: 0.1687 - val_macro_f1: 0.3149
Epoch 4/30
78/78 [==============================] - 20s 248ms/step - loss: 0.3876 - macro_double_f1: 0.6132 - accuracy: 0.7945 - balanced_accuracy: 0.6167 - double_precision: 0.6161 - double_recall: 0.6167 - aucroc: 0.6379 - aucpr: 0.2787 - cohen_kappa: 0.1845 - f1_score_macro: 0.3560 - f1_score_micro: 0.3816 - fbeta_score_macro: 0.3520 - fbeta_score_micro: 0.3749 - hamming_loss: 0.1901 - true_positive_rate: 0.3656 - true_negative_rate: 0.8678 - positive_predictive_value: 0.3535 - negative_predictive_value: 0.8787 - false_negative_rate: 0.6344 - false_positive_rate: 0.1322 - false_discovery_rate: 0.6465 - false_omission_rate: 0.1213 - threat_score: 0.2201 - fowlkes_mallows_index: 0.3566 - matthews_correlation: 0.2314 - informedness: 0.2335 - markedness: 0.2321 - macro_f1: 0.3537 - val_loss: 0.4174 - val_macro_double_f1: 0.5830 - val_accuracy: 0.7798 - val_balanced_accuracy: 0.5929 - val_double_precision: 0.5834 - val_double_recall: 0.5929 - val_aucroc: 0.6147 - val_aucpr: 0.2500 - val_cohen_kappa: 0.1681 - val_f1_score_macro: 0.3066 - val_f1_score_micro: 0.3481 - val_fbeta_score_macro: 0.2985 - val_fbeta_score_micro: 0.3387 - val_hamming_loss: 0.1984 - val_true_positive_rate: 0.3299 - val_true_negative_rate: 0.8558 - val_positive_predictive_value: 0.2937 - val_negative_predictive_value: 0.8732 - val_false_negative_rate: 0.6701 - val_false_positive_rate: 0.1442 - val_false_discovery_rate: 0.7063 - val_false_omission_rate: 0.1268 - val_threat_score: 0.1860 - val_fowlkes_mallows_index: 0.3072 - val_matthews_correlation: 0.1747 - val_informedness: 0.1857 - val_markedness: 0.1669 - val_macro_f1: 0.3027
Epoch 5/30
78/78 [==============================] - 20s 252ms/step - loss: 0.3856 - macro_double_f1: 0.6151 - accuracy: 0.7912 - balanced_accuracy: 0.6213 - double_precision: 0.6165 - double_recall: 0.6213 - aucroc: 0.6385 - aucpr: 0.2778 - cohen_kappa: 0.1864 - f1_score_macro: 0.3619 - f1_score_micro: 0.3867 - fbeta_score_macro: 0.3524 - fbeta_score_micro: 0.3747 - hamming_loss: 0.1908 - true_positive_rate: 0.3814 - true_negative_rate: 0.8611 - positive_predictive_value: 0.3526 - negative_predictive_value: 0.8804 - false_negative_rate: 0.6186 - false_positive_rate: 0.1389 - false_discovery_rate: 0.6474 - false_omission_rate: 0.1196 - threat_score: 0.2247 - fowlkes_mallows_index: 0.3635 - matthews_correlation: 0.2363 - informedness: 0.2426 - markedness: 0.2330 - macro_f1: 0.3602 - val_loss: 0.4121 - val_macro_double_f1: 0.5879 - val_accuracy: 0.7907 - val_balanced_accuracy: 0.5898 - val_double_precision: 0.5981 - val_double_recall: 0.5898 - val_aucroc: 0.6071 - val_aucpr: 0.2533 - val_cohen_kappa: 0.1557 - val_f1_score_macro: 0.3105 - val_f1_score_micro: 0.3550 - val_fbeta_score_macro: 0.3176 - val_fbeta_score_micro: 0.3536 - val_hamming_loss: 0.1971 - val_true_positive_rate: 0.3108 - val_true_negative_rate: 0.8688 - val_positive_predictive_value: 0.3240 - val_negative_predictive_value: 0.8722 - val_false_negative_rate: 0.6892 - val_false_positive_rate: 0.1312 - val_false_discovery_rate: 0.6760 - val_false_omission_rate: 0.1278 - val_threat_score: 0.1894 - val_fowlkes_mallows_index: 0.3117 - val_matthews_correlation: 0.1854 - val_informedness: 0.1797 - val_markedness: 0.1962 - val_macro_f1: 0.3063
Epoch 6/30
78/78 [==============================] - 20s 250ms/step - loss: 0.3786 - macro_double_f1: 0.6220 - accuracy: 0.7981 - balanced_accuracy: 0.6268 - double_precision: 0.6250 - double_recall: 0.6268 - aucroc: 0.6459 - aucpr: 0.2862 - cohen_kappa: 0.2055 - f1_score_macro: 0.3719 - f1_score_micro: 0.3976 - fbeta_score_macro: 0.3649 - fbeta_score_micro: 0.3887 - hamming_loss: 0.1861 - true_positive_rate: 0.3853 - true_negative_rate: 0.8683 - positive_predictive_value: 0.3678 - negative_predictive_value: 0.8822 - false_negative_rate: 0.6147 - false_positive_rate: 0.1317 - false_discovery_rate: 0.6322 - false_omission_rate: 0.1178 - threat_score: 0.2317 - fowlkes_mallows_index: 0.3729 - matthews_correlation: 0.2500 - informedness: 0.2537 - markedness: 0.2500 - macro_f1: 0.3693 - val_loss: 0.4165 - val_macro_double_f1: 0.5844 - val_accuracy: 0.7769 - val_balanced_accuracy: 0.5955 - val_double_precision: 0.5860 - val_double_recall: 0.5955 - val_aucroc: 0.6104 - val_aucpr: 0.2467 - val_cohen_kappa: 0.1650 - val_f1_score_macro: 0.3096 - val_f1_score_micro: 0.3415 - val_fbeta_score_macro: 0.3022 - val_fbeta_score_micro: 0.3317 - val_hamming_loss: 0.2002 - val_true_positive_rate: 0.3364 - val_true_negative_rate: 0.8545 - val_positive_predictive_value: 0.2998 - val_negative_predictive_value: 0.8722 - val_false_negative_rate: 0.6636 - val_false_positive_rate: 0.1455 - val_false_discovery_rate: 0.7002 - val_false_omission_rate: 0.1278 - val_threat_score: 0.1876 - val_fowlkes_mallows_index: 0.3120 - val_matthews_correlation: 0.1787 - val_informedness: 0.1910 - val_markedness: 0.1720 - val_macro_f1: 0.3064
24/24 [==============================] - 4s 148ms/step - loss: 0.4210 - macro_double_f1: 0.5794 - accuracy: 0.7728 - balanced_accuracy: 0.5908 - double_precision: 0.5801 - double_recall: 0.5908 - aucroc: 0.6056 - aucpr: 0.2406 - cohen_kappa: 0.1566 - f1_score_macro: 0.3008 - f1_score_micro: 0.3323 - fbeta_score_macro: 0.2926 - fbeta_score_micro: 0.3226 - hamming_loss: 0.2033 - true_positive_rate: 0.3306 - true_negative_rate: 0.8510 - positive_predictive_value: 0.2899 - negative_predictive_value: 0.8703 - false_negative_rate: 0.6694 - false_positive_rate: 0.1490 - false_discovery_rate: 0.7101 - false_omission_rate: 0.1297 - threat_score: 0.1821 - fowlkes_mallows_index: 0.3045 - matthews_correlation: 0.1686 - informedness: 0.1817 - markedness: 0.1602 - macro_f1: 0.2992
2021-06-17 00:29:33.130724: W tensorflow/python/util/util.cc:348] Sets are not currently considered sequences, but this may change in the future, so consider avoiding using them.

Process finished with exit code 0
