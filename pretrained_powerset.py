import tensorflow as tf
import tensorflow_addons as tfa
from tensorflow_hub import KerasLayer

from Utils.metrics import macro_f1, macro_double_f1, double_precision, double_recall, balanced_accuracy, \
    accuracy, true_positive_rate, true_negative_rate, positive_predictive_value, negative_predictive_value, \
    false_negative_rate, false_positive_rate, false_discovery_rate, false_omission_rate, threat_score, \
    fowlkes_mallows_index, matthews_correlation, informedness, markedness
from Utils.utils import macro_soft_f1, perf_grid, macro_double_soft_f1, \
    confusion_matrix, mcc_loss, cohens_kappa_loss
from common import Mode, load_data, tf_dataset, save_results, IMG_SIZE, stoppers, get_tuner, get_model, NetworkType, \
    decode, genres_dict, arr_to_str

HYPER_MODE = False
AUGMENT = False

init_bias = None


def static_model_f1(metrics):
    from common import num_classes
    return get_model(activation='elu',
                     use_dropout=True,
                     dropout_rate=.7,
                     use_bn=False,
                     bn_momentum=.7,
                     bn_center_scale=False,
                     learning_rate=.013579510904111872,
                     use_momentum=True,
                     momentum=.9702770080159557,
                     nesterov=False,
                     amsgrad=False,
                     initial_accumulator_value=.01,
                     centered=True,
                     grad_averaging=True,
                     optimizer='SGD',
                     hidden_count=2,
                     hidden_sizes=[312, 104],
                     loss=macro_double_soft_f1,
                     init_bias=init_bias,
                     output_size=num_classes,
                     output_activation='softmax',
                     metrics=metrics,
                     ntype=NetworkType.PRETRAINED)


def static_model_mcc(metrics):
    from common import num_classes
    return get_model(activation='relu',
                     use_dropout=True,
                     dropout_rate=.4,
                     use_bn=False,
                     bn_momentum=.65,
                     bn_center_scale=True,
                     learning_rate=.0070032622783436005,
                     use_momentum=False,
                     momentum=.9893319606625278,
                     nesterov=True,
                     amsgrad=True,
                     initial_accumulator_value=.1,
                     centered=False,
                     grad_averaging=True,
                     optimizer='LAMB',
                     hidden_count=2,
                     hidden_sizes=[216, 248],
                     loss=mcc_loss,
                     init_bias=init_bias,
                     output_size=num_classes,
                     output_activation='softmax',
                     metrics=metrics,
                     ntype=NetworkType.PRETRAINED)


def hyper_model(hp):
    from common import num_classes
    a = hp.Choice('activation', values=['selu', 'elu', 'gelu', 'relu'])
    ds = hp.Boolean('use_dropout')
    dr = hp.Float('dropout_rate', .2, .8, .1)
    bns = hp.Boolean('use_bn')
    bnm = hp.Float('bn_momentum', .5, .95, .05)
    bnc = hp.Boolean('bn_center_scale')

    ### optimizer
    lr = hp.Float('learning_rate', 1e-5, 1e-1)
    ms = hp.Boolean('use_momentum')
    mr = hp.Float('momentum', .9, .999)
    ns = hp.Boolean('nesterov')
    amss = hp.Boolean('amsgrad')
    iac = hp.Choice('initial_accumulator_value', values=[.01, .1])
    cs = hp.Boolean('centered')
    gas = hp.Boolean('grad_averaging')
    os = hp.Choice('optimizer', values=['Adadelta', 'Adagrad', 'Adam', 'Adamax', 'Ftrl', 'Nadam', 'RMSprop',
                                        'SGD', 'LAMB', 'LazyAdam', 'NovoGrad', 'ProximalAdagrad', 'Yogi'])
    if os == 'Adadelta':
        o = tf.keras.optimizers.Adadelta(lr)
    elif os == 'Adagrad':
        o = tf.keras.optimizers.Adagrad(lr, initial_accumulator_value=iac)
    elif os == 'Adam':
        o = tf.keras.optimizers.Adam(lr, amsgrad=amss)
    elif os == 'Adamax':
        o = tf.keras.optimizers.Adamax(lr)
    elif os == 'Ftrl':
        o = tf.keras.optimizers.Ftrl(lr, initial_accumulator_value=iac)
    elif os == 'Nadam':
        o = tf.keras.optimizers.Nadam(lr)
    elif os == 'RMSprop':
        o = tf.keras.optimizers.RMSprop(lr, momentum=mr if ms else .0, centered=cs)
    elif os == 'SGD':
        o = tf.keras.optimizers.SGD(lr, momentum=mr if ms else .0, nesterov=ns)
    elif os == 'LAMB':
        o = tfa.optimizers.LAMB(lr)
    elif os == 'LazyAdam':
        o = tfa.optimizers.LazyAdam(lr, amsgrad=amss)
    elif os == 'NovoGrad':
        o = tfa.optimizers.NovoGrad(lr, amsgrad=amss, grad_averaging=gas)
    elif os == 'ProximalAdagrad':
        o = tfa.optimizers.ProximalAdagrad(lr, initial_accumulator_value=iac)
    elif os == 'Yogi':
        o = tfa.optimizers.Yogi(lr)
    else:
        raise Exception('Wrong optimizer choice')

    hc = hp.Int('hidden_count', 1, 5)

    model = tf.keras.Sequential()
    model.add(KerasLayer(
        'https://tfhub.dev/google/imagenet/mobilenet_v2_100_224/feature_vector/4',
        input_shape=(IMG_SIZE, IMG_SIZE, 3),
        trainable=False,
        name='input_pretrained'
    ))
    for i in range(hc):
        model.add(tf.keras.layers.Dense(
            hp.Int(f'units{i}', 8, 376, 16), name=f'hidden{i}',
            activation=a, kernel_initializer='lecun_normal' if a == 'selu' else None))
        if bns and i != hc - 1:
            model.add(tf.keras.layers.BatchNormalization(momentum=bnm, center=bnc, scale=bnc))
        if ds:
            if a == 'selu':
                model.add(tf.keras.layers.AlphaDropout(dr))
            else:
                model.add(tf.keras.layers.Dropout(dr))
    model.add(tf.keras.layers.Dense(num_classes, activation='softmax', name='output',
                                    bias_initializer=tf.keras.initializers.Constant(init_bias)))

    model.compile(
        optimizer=o,
        loss=macro_double_soft_f1,
        metrics=[macro_double_f1, balanced_accuracy]
    )

    return model


def main():
    global init_bias
    train_x, test_x, val_x, val_y, train_y, test_y, (b, e), init_bias = load_data(Mode.MULTI_CLASS)
    train_ds = tf_dataset(train_x, train_y, True)
    val_ds = tf_dataset(val_x, val_y, False)
    test_ds = tf_dataset(test_x, test_y, False)

    from common import num_classes
    metrics = [macro_double_f1, accuracy, balanced_accuracy, double_precision, double_recall,
               tf.keras.metrics.AUC(multi_label=False, curve='ROC', name='aucroc'),
               tf.keras.metrics.AUC(multi_label=False, curve='PR', name='aucpr'),
               tfa.metrics.CohenKappa(num_classes),
               tfa.metrics.F1Score(num_classes, 'macro', .5, name='f1_score_macro'),
               tfa.metrics.F1Score(num_classes, 'micro', .5, name='f1_score_micro'),
               tfa.metrics.FBetaScore(num_classes, 'macro', .5, .5, name='fbeta_score_macro'),
               tfa.metrics.FBetaScore(num_classes, 'micro', .5, .5, name='fbeta_score_micro'),
               tfa.metrics.HammingLoss('multiclass'),
               tfa.losses.ContrastiveLoss(),
               true_positive_rate, true_negative_rate, positive_predictive_value, negative_predictive_value,
               false_negative_rate, false_positive_rate, false_discovery_rate, false_omission_rate,
               threat_score, fowlkes_mallows_index, matthews_correlation, informedness, markedness,
               macro_f1]

    if HYPER_MODE:
        tuner = get_tuner(hyper_model, 'PRTRND-POW', 'Hyperband')
        tuner.search(train_ds, epochs=50, validation_data=val_ds, callbacks=stoppers)
        tuner.results_summary()
        best_hps = tuner.get_best_hyperparameters(num_trials=1)[0]
        model = tuner.hypermodel.build(best_hps)
        model.compile(optimizer=model.optimizer, loss=model.loss,
                      metrics=metrics)
        model.summary()
    else:
        model = static_model_f1(metrics)

    model.evaluate(test_ds)
    history = model.fit(
        train_ds,
        epochs=30,
        validation_data=val_ds,
        callbacks=stoppers
    )

    save_results(
        ['loss', 'macro_double_f1', 'accuracy', 'balanced_accuracy', 'double_precision', 'double_recall',
         'aucroc', 'aucpr', 'cohen_kappa', 'f1_score_macro', 'f1_score_micro',
         'fbeta_score_macro', 'fbeta_score_micro', 'hamming_loss', 'contrastive_loss',
         'true_positive_rate', 'true_negative_rate', 'positive_predictive_value', 'negative_predictive_value',
         'false_negative_rate', 'false_positive_rate', 'false_discovery_rate', 'false_omission_rate',
         'threat_score', 'fowlkes_mallows_index', 'matthews_correlation', 'informedness', 'markedness',
         'macro_f1'],
        history, model, test_ds, test_y, [arr_to_str([genres_dict[z] for z in y]) for x in e.categories_[0]
                                          for y in b.inverse_transform(decode(x, len(b.classes_)))]
    )


main()
