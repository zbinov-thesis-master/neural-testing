import tensorflow as tf
import tensorflow_addons as tfa

from Utils.metrics import macro_double_f1, accuracy, balanced_accuracy, double_precision, double_recall, \
    true_positive_rate, true_negative_rate, positive_predictive_value, negative_predictive_value, false_negative_rate, \
    false_positive_rate, false_discovery_rate, false_omission_rate, threat_score, fowlkes_mallows_index, \
    matthews_correlation, informedness, markedness, macro_f1
from Utils.utils import macro_double_soft_f1, mcc_loss
from common import get_tuner, stoppers, save_results, genres_dict, tf_dataset, load_data, Mode, IMG_SIZE, get_model_cnn

HYPER_MODE = False
AUGMENT = 0

init_bias = None


def static_model_f1(metrics):
    from common import num_classes
    return get_model_cnn(activation='relu',
                         use_dropout=False,
                         dropout_rate=.4,
                         use_bn=True,
                         bn_momentum=.6,
                         bn_center_scale=True,
                         learning_rate=.006055248266370699,
                         use_momentum=True,
                         momentum=.9630266541541634,
                         nesterov=False,
                         amsgrad=False,
                         initial_accumulator_value=.1,
                         centered=True,
                         grad_averaging=False,
                         optimizer='NovoGrad',
                         hidden_count=2,
                         hidden_sizes=[72, 104],
                         convpool_count=2,
                         convpool_sizes=[1, 2],
                         conv_filter_start_size=8,
                         kernel_size=(5, 5),
                         output_size=num_classes,
                         init_bias=init_bias,
                         loss=macro_double_soft_f1,
                         output_activation='sigmoid',
                         metrics=metrics)


def static_model_mcc(metrics):
    from common import num_classes
    return get_model_cnn(activation='selu',
                         use_dropout=False,
                         dropout_rate=.6,
                         use_bn=True,
                         bn_momentum=.5,
                         bn_center_scale=False,
                         learning_rate=.0023746046796039384,
                         use_momentum=False,
                         momentum=.9029190415113933,
                         nesterov=False,
                         amsgrad=True,
                         initial_accumulator_value=.01,
                         centered=True,
                         grad_averaging=False,
                         optimizer='RMSprop',
                         hidden_count=3,
                         hidden_sizes=[56, 40, 24],
                         convpool_count=3,
                         convpool_sizes=[2, 3, 1],
                         conv_filter_start_size=16,
                         kernel_size=(5, 5),
                         output_size=num_classes,
                         init_bias=init_bias,
                         loss=macro_double_soft_f1,
                         output_activation='sigmoid',
                         metrics=metrics)


def hyper_model(hp):
    from common import num_classes
    a = hp.Choice('activation', values=['selu', 'elu', 'gelu', 'relu'])
    ds = hp.Boolean('use_dropout')
    dr = hp.Float('dropout_rate', .2, .8, .1)
    bns = hp.Boolean('use_bn')
    bnm = hp.Float('bn_momentum', .5, .95, .05)
    bnc = hp.Boolean('bn_center_scale')

    ### optimizer
    lr = hp.Float('learning_rate', 1e-5, 1e-1)
    ms = hp.Boolean('use_momentum')
    mr = hp.Float('momentum', .9, .999)
    ns = hp.Boolean('nesterov')
    amss = hp.Boolean('amsgrad')
    iac = hp.Choice('initial_accumulator_value', values=[.01, .1])
    cs = hp.Boolean('centered')
    gas = hp.Boolean('grad_averaging')
    os = hp.Choice('optimizer', values=['Adadelta', 'Adagrad', 'Adam', 'Adamax', 'Ftrl', 'Nadam', 'RMSprop',
                                        'SGD', 'LAMB', 'LazyAdam', 'NovoGrad', 'ProximalAdagrad', 'Yogi'])
    if os == 'Adadelta':
        o = tf.keras.optimizers.Adadelta(lr)
    elif os == 'Adagrad':
        o = tf.keras.optimizers.Adagrad(lr, initial_accumulator_value=iac)
    elif os == 'Adam':
        o = tf.keras.optimizers.Adam(lr, amsgrad=amss)
    elif os == 'Adamax':
        o = tf.keras.optimizers.Adamax(lr)
    elif os == 'Ftrl':
        o = tf.keras.optimizers.Ftrl(lr, initial_accumulator_value=iac)
    elif os == 'Nadam':
        o = tf.keras.optimizers.Nadam(lr)
    elif os == 'RMSprop':
        o = tf.keras.optimizers.RMSprop(lr, momentum=mr if ms else .0, centered=cs)
    elif os == 'SGD':
        o = tf.keras.optimizers.SGD(lr, momentum=mr if ms else .0, nesterov=ns)
    elif os == 'LAMB':
        o = tfa.optimizers.LAMB(lr)
    elif os == 'LazyAdam':
        o = tfa.optimizers.LazyAdam(lr, amsgrad=amss)
    elif os == 'NovoGrad':
        o = tfa.optimizers.NovoGrad(lr, amsgrad=amss, grad_averaging=gas)
    elif os == 'ProximalAdagrad':
        o = tfa.optimizers.ProximalAdagrad(lr, initial_accumulator_value=iac)
    elif os == 'Yogi':
        o = tfa.optimizers.Yogi(lr)
    else:
        raise Exception('Wrong optimizer choice')

    hc = hp.Int('hidden_count', 1, 3)
    cc = hp.Int('convpool_count', 1, 3)
    cf = hp.Choice('conv_filters_start_count', values=[8, 16])
    kn = hp.Int('kernel_size', 3, 5, 2)

    model = tf.keras.Sequential()
    model.add(tf.keras.layers.Conv2D(input_shape=(IMG_SIZE, IMG_SIZE, 3),
              filters=cf, kernel_size=kn, activation='relu', padding='same', name='conv0_0'))
    for c in range(1, hp.Int('conv0_count', 1, 3)):
        model.add(tf.keras.layers.Conv2D(
            cf, (3, 3), activation='relu', padding='same', name=f'conv0_{c}')
        )
    model.add(tf.keras.layers.MaxPool2D(name='pooling0'))
    if bns:
        model.add(tf.keras.layers.BatchNormalization(momentum=bnm, center=bnc, scale=bnc))
    if ds:
        model.add(tf.keras.layers.Dropout(dr))
    for p in range(1, cc):
        for c in range(hp.Int(f'conv{p}_count', 1, 3)):
            model.add(tf.keras.layers.Conv2D(
                (2 ** p) * cf, kn, activation='relu', padding='same', name=f'conv{p}_{c}')
            )
        model.add(tf.keras.layers.MaxPool2D(name=f'pooling{p}'))
        if bns:
            model.add(tf.keras.layers.BatchNormalization(momentum=bnm, center=bnc, scale=bnc))
        if ds:
            model.add(tf.keras.layers.Dropout(dr))
    model.add(tf.keras.layers.Flatten(name='flatten'))
    for h in range(hc):
        model.add(tf.keras.layers.Dense(
            hp.Int(f'units{h}', 8, 64, 16), name=f'hidden{h}',
            activation=a, kernel_initializer='lecun_normal' if a == 'selu' else None))
        if bns and h != hc - 1:
            model.add(tf.keras.layers.BatchNormalization(momentum=bnm, center=bnc, scale=bnc))
        if ds:
            if a == 'selu':
                model.add(tf.keras.layers.AlphaDropout(dr))
            else:
                model.add(tf.keras.layers.Dropout(dr))
    model.add(tf.keras.layers.Dense(num_classes, activation='sigmoid', name='output',
                                    bias_initializer=tf.keras.initializers.Constant(init_bias)))

    model.compile(
        optimizer=o,
        loss=macro_double_soft_f1,
        metrics=[balanced_accuracy]
    )

    return model


def main():
    global init_bias
    train_x, test_x, val_x, val_y, train_y, test_y, binarizer, init_bias = load_data(Mode.MULTI_LABEL)
    train_ds = tf_dataset(train_x, train_y, True, AUGMENT)
    val_ds = tf_dataset(val_x, val_y, False)
    test_ds = tf_dataset(test_x, test_y, False)

    from common import num_classes
    metrics = [macro_double_f1, accuracy, balanced_accuracy, double_precision, double_recall,
               tf.keras.metrics.AUC(multi_label=True, curve='ROC', name='aucroc'),
               tf.keras.metrics.AUC(multi_label=True, curve='PR', name='aucpr'),
               tfa.metrics.CohenKappa(num_classes),
               tfa.metrics.F1Score(num_classes, 'macro', .5, name='f1_score_macro'),
               tfa.metrics.F1Score(num_classes, 'micro', .5, name='f1_score_micro'),
               tfa.metrics.FBetaScore(num_classes, 'macro', .5, .5, name='fbeta_score_macro'),
               tfa.metrics.FBetaScore(num_classes, 'micro', .5, .5, name='fbeta_score_micro'),
               tfa.metrics.HammingLoss('multilabel', threshold=.5),
               tfa.losses.ContrastiveLoss(),
               true_positive_rate, true_negative_rate, positive_predictive_value, negative_predictive_value,
               false_negative_rate, false_positive_rate, false_discovery_rate, false_omission_rate,
               threat_score, fowlkes_mallows_index, matthews_correlation, informedness, markedness,
               macro_f1]

    if HYPER_MODE:
        tuner = get_tuner(hyper_model, 'CNN-BIN', 'Hyperband')
        tuner.search(train_ds, epochs=50, validation_data=val_ds, callbacks=stoppers)
        tuner.results_summary()
        best_hps = tuner.get_best_hyperparameters(num_trials=1)[0]
        model = tuner.hypermodel.build(best_hps)
        model.compile(optimizer=model.optimizer, loss=model.loss,
                      metrics=metrics)
    else:
        model = static_model_f1(metrics)
    model.summary()

    model.evaluate(test_ds)
    history = model.fit(
        train_ds,
        epochs=30,
        validation_data=val_ds,
        callbacks=stoppers
    )

    save_results(
        ['loss', 'macro_double_f1', 'accuracy', 'balanced_accuracy', 'double_precision', 'double_recall',
         'aucroc', 'aucpr', 'cohen_kappa', 'f1_score_macro', 'f1_score_micro',
         'fbeta_score_macro', 'fbeta_score_micro', 'hamming_loss', 'contrastive_loss',
         'true_positive_rate', 'true_negative_rate', 'positive_predictive_value', 'negative_predictive_value',
         'false_negative_rate', 'false_positive_rate', 'false_discovery_rate', 'false_omission_rate',
         'threat_score', 'fowlkes_mallows_index', 'matthews_correlation', 'informedness', 'markedness',
         'macro_f1'],
        history, model, test_ds, test_y, [genres_dict[x] for x in binarizer.classes_]
    )


main()
