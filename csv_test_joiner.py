from pandas import read_csv, DataFrame


BASE_RESULTS_DIR = 'P:/meng'
MODEL_SUBDIRS = ['cnn_binary',
                 'cnn_binary+x2augmentation',
                 'cnn_binary+x4augmentation',
                 'custom_binary',
                 'custom_powerset',
                 'pretrained_binary',
                 'pretrained_powerset']
LOSS_SUBDIRS = ['double_f1',
                'mcc',
                'bce']


translation = {
    'training': 'trening',
    'validation': 'walidacja',
    'epoch': 'epoka',
    'target': 'cel',
    'true': 'rzeczywista',
    'prediction': 'predykcja',
    'label': 'etykieta',
    'average occurrence': 'średnia częstotliwość występowania',
    'TP rate': 'prawdziwie pozytywna stopa',
    'FP rate': 'fałszywie pozytywna stopa',
    'accuracy': 'dokładność',
    'aucpr': 'AUC PR',
    'aucroc': 'AUC ROC',
    'auc_pr': 'AUC PR',
    'auc_roc': 'AUC ROC',
    'balanced_accuracy': 'zbalansowana dokładność',
    'cohen_kappa': 'kappa Cohena',
    'contrastive_loss': 'ranking loss', #
    'double_precision': 'dwustronna precyzja',
    'double_recall': 'dwustronna czułość',
    'f1_score_macro': 'f1 makrouśredniane',
    'f1_score_micro': 'f1 mikrouśredniane',
    'false_discovery_rate': 'współczynnik fałszywego wykrycia',
    'false_negative_rate': 'współczynnik wyników fałszywie negatywnych',
    'false_omission_rate': 'współczynnik fałszywego pominięcia',
    'false_positive_rate': 'współczynnik wyników fałszywie pozytywnych',
    'fbeta_score_macro': 'fβ makrouśredniane',
    'fbeta_score_micro': 'fβ mikrouśredniane',
    'fowlkes_mallows_index': 'indeks Fowlkes-Mallows',
    'hamming_loss': 'Hamming loss',
    'informedness': 'informedness', #
    'loss': 'strata',
    'macro_double_f1': 'dwustronne f1 makrouśredniane',
    'macro_f1': 'f1 makrouśredniane',
    'markedness': 'markedness', #
    'matthews_correlation': 'współczynnik korelacji Matthew',
    'negative_predictive_value': 'negatywna wartość prognostyczna',
    'positive_predictive_value': 'pozytywna wartość prognostyczna',
    'threat_score': 'threat_score', #
    'true_negative_rate': 'współczynnik wyników prawdziwie negatywnych',
    'true_positive_rate': 'współczynnik wyników prawdziwie pozytywnych',
    'precision': 'precyzja',
    'recall': 'czułość'
}


def main():
    data = dict()
    missing = []
    for m in MODEL_SUBDIRS:
        for l in LOSS_SUBDIRS:
            try:
                data[m, l] = read_csv(f'{BASE_RESULTS_DIR}/{m}/{l}/_joined/test_eval.csv', float_precision='%.3f')
            except FileNotFoundError:
                missing.append(f'{m}/{l}')
    group_by_model = DataFrame(data[MODEL_SUBDIRS[0], LOSS_SUBDIRS[0]].iloc[:, 0])
    # group_by_model['metric'] = group_by_model['metric'].apply(lambda x: translation[x])  # TRANSLATE
    for m in MODEL_SUBDIRS:
        for l in LOSS_SUBDIRS:
            try:
                group_by_model[f'{m} {l}'] = data[m, l]['score']
                group_by_model[f'{m} {l} std'] = data[m, l]['std']
            except KeyError as e:
                if f'{m}/{l}' not in missing:
                    raise Exception(e)
    group_by_model.to_csv(f'{BASE_RESULTS_DIR}/test_eval_by_model.csv', index=False, float_format='%.3f')

    if missing:
        print('Some directories could not be found:\n', '\n '.join(missing))


main()
