from tensorflow import function, cast, greater, float32, math, reduce_mean


@function
def macro_f1(y_true, y_pred, threshold=0.5):
    y_pred = cast(greater(y_pred, threshold), float32)
    tp = cast(math.count_nonzero(y_pred * y_true, axis=0), float32)
    fp = cast(math.count_nonzero(y_pred * (1 - y_true), axis=0), float32)
    fn = cast(math.count_nonzero((1 - y_pred) * y_true, axis=0), float32)

    f1 = 2 * tp / (2 * tp + fn + fp + 1e-16)
    return reduce_mean(f1)


@function
def macro_double_f1(y_true, y_pred, threshold=0.5):
    y_pred = cast(greater(y_pred, threshold), float32)
    tp = cast(math.count_nonzero(y_pred * y_true, axis=0), float32)
    tn = cast(math.count_nonzero((1 - y_pred) * (1 - y_true), axis=0), float32)
    fp = cast(math.count_nonzero(y_pred * (1 - y_true), axis=0), float32)
    fn = cast(math.count_nonzero((1 - y_pred) * y_true, axis=0), float32)

    f1_1 = 2 * tp / (2 * tp + fn + fp + 1e-16)
    f1_0 = 2 * tn / (2 * tn + fn + fp + 1e-16)

    f1 = 0.5 * (f1_1 + f1_0)
    return reduce_mean(f1)


@function
def accuracy(y_true, y_pred, threshold=0.5):
    y_pred = cast(greater(y_pred, threshold), float32)
    tp = cast(math.count_nonzero(y_pred * y_true, axis=0), float32)
    tn = cast(math.count_nonzero((1 - y_pred) * (1 - y_true), axis=0), float32)
    fp = cast(math.count_nonzero(y_pred * (1 - y_true), axis=0), float32)
    fn = cast(math.count_nonzero((1 - y_pred) * y_true, axis=0), float32)

    a = (tp + tn) / (tp + tn + fp + fn + 1e-16)
    return reduce_mean(a)


@function
def balanced_accuracy(y_true, y_pred, threshold=0.5):
    y_pred = cast(greater(y_pred, threshold), float32)
    tp = cast(math.count_nonzero(y_pred * y_true, axis=0), float32)
    tn = cast(math.count_nonzero((1 - y_pred) * (1 - y_true), axis=0), float32)
    fp = cast(math.count_nonzero(y_pred * (1 - y_true), axis=0), float32)
    fn = cast(math.count_nonzero((1 - y_pred) * y_true, axis=0), float32)

    tpr = tp / (tp + fn + 1e-16)
    tnr = tn / (tn + fp + 1e-16)

    a = .5 * (tpr + tnr)
    return reduce_mean(a)


@function
def double_precision(y_true, y_pred, threshold=0.5):
    y_pred = cast(greater(y_pred, threshold), float32)
    tp = cast(math.count_nonzero(y_pred * y_true, axis=0), float32)
    tn = cast(math.count_nonzero((1 - y_pred) * (1 - y_true), axis=0), float32)
    fp = cast(math.count_nonzero(y_pred * (1 - y_true), axis=0), float32)
    fn = cast(math.count_nonzero((1 - y_pred) * y_true, axis=0), float32)

    p_1 = tp / (tp + fp + 1e-16)
    p_0 = tn / (tn + fn + 1e-16)

    p = 0.5 * (p_1 + p_0)
    return reduce_mean(p)


@function
def double_recall(y_true, y_pred, threshold=0.5):
    y_pred = cast(greater(y_pred, threshold), float32)
    tp = cast(math.count_nonzero(y_pred * y_true, axis=0), float32)
    tn = cast(math.count_nonzero((1 - y_pred) * (1 - y_true), axis=0), float32)
    fp = cast(math.count_nonzero(y_pred * (1 - y_true), axis=0), float32)
    fn = cast(math.count_nonzero((1 - y_pred) * y_true, axis=0), float32)

    r_1 = tp / (tp + fn + 1e-16)
    r_0 = tn / (tn + fp + 1e-16)

    r = 0.5 * (r_1 + r_0)
    return reduce_mean(r)


@function   # sensitivity, recall
def true_positive_rate(y_true, y_pred, threshold=0.5):
    y_pred = cast(greater(y_pred, threshold), float32)
    tp = cast(math.count_nonzero(y_pred * y_true, axis=0), float32)
    fn = cast(math.count_nonzero((1 - y_pred) * y_true, axis=0), float32)

    tpr = tp / (tp + fn + 1e-16)
    return reduce_mean(tpr)


@function   # specificity, selectivity
def true_negative_rate(y_true, y_pred, threshold=0.5):
    y_pred = cast(greater(y_pred, threshold), float32)
    tn = cast(math.count_nonzero((1 - y_pred) * (1 - y_true), axis=0), float32)
    fp = cast(math.count_nonzero(y_pred * (1 - y_true), axis=0), float32)

    tnr = tn / (tn + fp + 1e-16)
    return reduce_mean(tnr)


@function   # precision
def positive_predictive_value(y_true, y_pred, threshold=0.5):
    y_pred = cast(greater(y_pred, threshold), float32)
    tp = cast(math.count_nonzero(y_pred * y_true, axis=0), float32)
    fp = cast(math.count_nonzero(y_pred * (1 - y_true), axis=0), float32)

    ppv = tp / (tp + fp + 1e-16)
    return reduce_mean(ppv)


@function
def negative_predictive_value(y_true, y_pred, threshold=0.5):
    y_pred = cast(greater(y_pred, threshold), float32)
    tn = cast(math.count_nonzero((1 - y_pred) * (1 - y_true), axis=0), float32)
    fn = cast(math.count_nonzero((1 - y_pred) * y_true, axis=0), float32)

    npv = tn / (tn + fn + 1e-16)
    return reduce_mean(npv)


@function   # miss rate
def false_negative_rate(y_true, y_pred, threshold=0.5):
    y_pred = cast(greater(y_pred, threshold), float32)
    tp = cast(math.count_nonzero(y_pred * y_true, axis=0), float32)
    fn = cast(math.count_nonzero((1 - y_pred) * y_true, axis=0), float32)

    fnr = fn / (fn + tp + 1e-16)
    return reduce_mean(fnr)


@function   # fall-out
def false_positive_rate(y_true, y_pred, threshold=0.5):
    y_pred = cast(greater(y_pred, threshold), float32)
    tn = cast(math.count_nonzero((1 - y_pred) * (1 - y_true), axis=0), float32)
    fp = cast(math.count_nonzero(y_pred * (1 - y_true), axis=0), float32)

    fpr = fp / (fp + tn + 1e-16)
    return reduce_mean(fpr)


@function
def false_discovery_rate(y_true, y_pred, threshold=0.5):
    y_pred = cast(greater(y_pred, threshold), float32)
    tp = cast(math.count_nonzero(y_pred * y_true, axis=0), float32)
    fp = cast(math.count_nonzero(y_pred * (1 - y_true), axis=0), float32)

    fdr = fp / (fp + tp + 1e-16)
    return reduce_mean(fdr)


@function
def false_omission_rate(y_true, y_pred, threshold=0.5):
    y_pred = cast(greater(y_pred, threshold), float32)
    tn = cast(math.count_nonzero((1 - y_pred) * (1 - y_true), axis=0), float32)
    fn = cast(math.count_nonzero((1 - y_pred) * y_true, axis=0), float32)

    for_ = fn / (fn + tn + 1e-16)
    return reduce_mean(for_)


@function   # critical success index
def threat_score(y_true, y_pred, threshold=0.5):
    y_pred = cast(greater(y_pred, threshold), float32)
    tp = cast(math.count_nonzero(y_pred * y_true, axis=0), float32)
    fp = cast(math.count_nonzero(y_pred * (1 - y_true), axis=0), float32)
    fn = cast(math.count_nonzero((1 - y_pred) * y_true, axis=0), float32)

    ts = tp / (tp + fn + fp + 1e-16)
    return reduce_mean(ts)


@function
def fowlkes_mallows_index(y_true, y_pred, threshold=0.5):
    y_pred = cast(greater(y_pred, threshold), float32)
    tp = cast(math.count_nonzero(y_pred * y_true, axis=0), float32)
    fp = cast(math.count_nonzero(y_pred * (1 - y_true), axis=0), float32)
    fn = cast(math.count_nonzero((1 - y_pred) * y_true, axis=0), float32)

    ppv = tp / (tp + fp + 1e-16)
    tpr = tp / (tp + fn + 1e-16)

    fm = math.sqrt(ppv * tpr)
    return reduce_mean(fm)


@function
def matthews_correlation(y_true, y_pred, threshold=0.5):
    y_pred = cast(greater(y_pred, threshold), float32)
    tp = cast(math.count_nonzero(y_pred * y_true, axis=0), float32)
    tn = cast(math.count_nonzero((1 - y_pred) * (1 - y_true), axis=0), float32)
    fp = cast(math.count_nonzero(y_pred * (1 - y_true), axis=0), float32)
    fn = cast(math.count_nonzero((1 - y_pred) * y_true, axis=0), float32)

    u = tp * tn - fp * fn
    d = math.sqrt((tp + fp) * (tp + fn) * (tn + fp) * (tn + fn))

    mcc = u / (d + 1e-16)
    return reduce_mean(mcc)


@function
def informedness(y_true, y_pred, threshold=0.5):
    y_pred = cast(greater(y_pred, threshold), float32)
    tp = cast(math.count_nonzero(y_pred * y_true, axis=0), float32)
    tn = cast(math.count_nonzero((1 - y_pred) * (1 - y_true), axis=0), float32)
    fp = cast(math.count_nonzero(y_pred * (1 - y_true), axis=0), float32)
    fn = cast(math.count_nonzero((1 - y_pred) * y_true, axis=0), float32)

    tpr = tp / (tp + fn + 1e-16)
    tnr = tn / (tn + fp + 1e-16)

    i = tpr + tnr - 1
    return reduce_mean(i)


@function
def markedness(y_true, y_pred, threshold=0.5):
    y_pred = cast(greater(y_pred, threshold), float32)
    tp = cast(math.count_nonzero(y_pred * y_true, axis=0), float32)
    tn = cast(math.count_nonzero((1 - y_pred) * (1 - y_true), axis=0), float32)
    fp = cast(math.count_nonzero(y_pred * (1 - y_true), axis=0), float32)
    fn = cast(math.count_nonzero((1 - y_pred) * y_true, axis=0), float32)

    ppv = tp / (tp + fp + 1e-16)
    npv = tn / (tn + fn + 1e-16)

    m = ppv + npv - 1
    return reduce_mean(m)
