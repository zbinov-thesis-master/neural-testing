from random import Random
from typing import List

from matplotlib import pyplot, style
from numpy import linspace, float32 as npfloat32, count_nonzero, expand_dims, intersect1d, mean, array, average
from pandas import DataFrame, Series
from sklearn.metrics import roc_curve, auc, precision_recall_curve, average_precision_score
from tensorflow import io, image, data, function, cast, int32, float32, reduce_sum, reduce_mean, greater, math, random
from tensorflow.keras.preprocessing.image import load_img, img_to_array

IMAGE_WIDTH = 224
IMAGE_HEIGHT = 224
IMAGE_CHANNELS = 3

BATCH_SIZE = 256
AUTOTUNE = data.AUTOTUNE
SHUFFLE_BUFFER_SIZE = 1024
RNG = random.Generator.from_seed(1337, alg='philox')
RANDOM = Random()


### data preparation ##############################################################################


def parse_image(filename: str, label: List[int]) -> (List[float], List[int]):
    img_string = io.read_file(filename)
    img_decoded = image.decode_jpeg(img_string, channels=IMAGE_CHANNELS)
    img_resized = image.resize(img_decoded, [IMAGE_WIDTH, IMAGE_HEIGHT])
    img_normalized = img_resized / 255.0
    return img_normalized, label


def augment(img: List[float], label: List[int]) -> (List[float], List[int]):
    seed = RNG.make_seeds(2)[0]
    imgx = image.stateless_random_flip_up_down(img, seed)
    imgx = image.stateless_random_flip_left_right(imgx, seed)
    imgx = image.stateless_random_jpeg_quality(imgx, 20, 50, seed)
    imgx = image.rot90(imgx, RANDOM.randint(0, 3))
    imgx = image.stateless_random_crop(imgx, [int(0.8 * IMAGE_HEIGHT), int(0.8 * IMAGE_WIDTH), 3], seed)
    imgx = image.resize(imgx, [IMAGE_WIDTH, IMAGE_HEIGHT])
    return imgx, label


def create_dataset(filenames: List[str], labels: List[int], is_training: bool, is_augmenting: bool = False):
    dataset = data.Dataset.from_tensor_slices((filenames, labels))
    dataset = dataset.map(parse_image, num_parallel_calls=AUTOTUNE)

    if is_training:
        if is_augmenting:
            augmentation = dataset.map(augment, num_parallel_calls=AUTOTUNE)
            dataset = dataset.concatenate(augmentation)

        dataset = dataset.shuffle(buffer_size=SHUFFLE_BUFFER_SIZE)

    dataset = dataset.batch(BATCH_SIZE)
    dataset = dataset.prefetch(buffer_size=AUTOTUNE)

    return dataset


### losses ########################################################################################


@function
def macro_soft_f1(y_true, y_pred):
    y_true = cast(y_true, float32)
    y_pred = cast(y_pred, float32)
    tp = reduce_sum(y_pred * y_true, axis=0)
    fp = reduce_sum(y_pred * (1 - y_true), axis=0)
    fn = reduce_sum((1 - y_pred) * y_true, axis=0)
    soft_f1 = 2 * tp / (2 * tp + fn + fp + 1e-16)
    cost = 1 - soft_f1
    return reduce_mean(cost)


@function
def macro_double_soft_f1(y_true, y_pred):
    y_true = cast(y_true, float32)
    y_pred = cast(y_pred, float32)
    tp = reduce_sum(y_pred * y_true, axis=0)
    tn = reduce_sum((1 - y_pred) * (1 - y_true), axis=0)
    fp = reduce_sum(y_pred * (1 - y_true), axis=0)
    fn = reduce_sum((1 - y_pred) * y_true, axis=0)
    soft_f1_1 = 2 * tp / (2 * tp + fn + fp + 1e-16)
    soft_f1_0 = 2 * tn / (2 * tn + fn + fp + 1e-16)
    cost_1 = 1 - soft_f1_1
    cost_0 = 1 - soft_f1_0
    cost = 0.5 * (cost_1 + cost_0)
    return reduce_mean(cost)


@function
def mcc_loss(y_true, y_pred):
    y_true = cast(y_true, float32)
    y_pred = cast(y_pred, float32)
    tp = reduce_sum(y_pred * y_true, axis=0)
    tn = reduce_sum((1 - y_pred) * (1 - y_true), axis=0)
    fp = reduce_sum(y_pred * (1 - y_true), axis=0)
    fn = reduce_sum((1 - y_pred) * y_true, axis=0)

    n = tp * tn - fp * fn
    d = math.sqrt((tp + fp) * (tp + fn) * (tn + fp) * (tn + fn))

    mcc = math.divide_no_nan(n, d)
    cost = -mcc + 1
    cost /= 2
    return reduce_mean(cost)


@function
def cohens_kappa_loss(y_true, y_pred):
    y_true = cast(y_true, float32)
    y_pred = cast(y_pred, float32)
    tp = reduce_sum(y_pred * y_true, axis=0)
    tn = reduce_sum((1 - y_pred) * (1 - y_true), axis=0)
    fp = reduce_sum(y_pred * (1 - y_true), axis=0)
    fn = reduce_sum((1 - y_pred) * y_true, axis=0)

    n = 2 * (tp * tn - fn * fp)
    d = (tp + fp) * (tn + fp) + (tp + fn) * (tn + fn)

    ck = math.divide_no_nan(n, d)
    cost = 1 - ck
    return reduce_mean(cost)


###################################################################################################


def learning_curves(history, test_loss, test_macro_f1):
    loss = history.history['loss']
    val_loss = history.history['val_loss']

    macro_f1 = history.history['macro_double_f1']
    val_macro_f1 = history.history['val_macro_double_f1']

    epochs = len(loss)

    style.use("bmh")
    pyplot.figure(figsize=(8, 8))

    pyplot.subplot(2, 1, 1)
    pyplot.plot(range(1, epochs + 1), loss, label='training')
    pyplot.plot(range(1, epochs + 1), val_loss, label='validation')
    pyplot.legend(loc='upper right')
    pyplot.ylabel('loss')
    pyplot.title('Loss')

    pyplot.subplot(2, 1, 2)
    pyplot.plot(range(1, epochs + 1), macro_f1, label='training')
    pyplot.plot(range(1, epochs + 1), val_macro_f1, label='validation')
    pyplot.legend(loc='lower right')
    pyplot.ylabel('macro F1-score')
    pyplot.title('Macro F1-score')
    pyplot.xlabel('epoch\n\n'
                  f'Test loss: {test_loss:.4f}  -  macro_f1: {test_macro_f1:.4f}')

    pyplot.tight_layout()
    pyplot.savefig('results/learning_curves')

    return loss, val_loss, macro_f1, val_macro_f1


def plot_metric(history, metric_name, test_score):
    metric = history.history[metric_name]
    val_metric = history.history[f'val_{metric_name}']

    epochs = len(metric)

    style.use('bmh')
    pyplot.figure(figsize=(8, 4))
    pyplot.plot(range(1, epochs + 1), metric, label='training')
    pyplot.plot(range(1, epochs + 1), val_metric, label='validation')
    pyplot.legend()
    pyplot.ylabel(metric_name)
    pyplot.title(f'Training and Validation {metric_name}')
    pyplot.xlabel('epoch\n\n'
                  f'Test score: {test_score:.4f}')

    pyplot.tight_layout()
    pyplot.savefig(f'results/{metric_name}')
    pyplot.close()

    df = DataFrame(array([metric, val_metric]).T, columns=['training', 'validation'])
    df.to_csv(f'results/{metric_name}.csv', index=False)

    return metric, val_metric


def plot_roc(y_pred, target):
    fpr, tpr, _ = roc_curve(target.ravel(), y_pred.ravel())

    auc_ = auc(fpr, tpr)

    pyplot.figure(figsize=(8, 8))
    pyplot.plot(fpr, tpr)
    pyplot.ylabel('TP rate')
    pyplot.title('ROC curve')
    pyplot.xlabel('FP rate\n\n'
                  f'AUC = {auc_}')

    pyplot.tight_layout()
    pyplot.savefig(f'results/roc_curve')

    df = DataFrame(array([fpr, tpr]).T, columns=['fpr', 'tpr'])
    df.to_csv(f'results/roc.csv', index=False)


def plot_pr(y_pred, target):
    p, r, _ = precision_recall_curve(target.ravel(), y_pred.ravel())

    auc_ = average_precision_score(target.ravel(), y_pred.ravel())

    pyplot.figure(figsize=(8, 8))
    pyplot.plot(r, p)
    pyplot.ylabel('precision')
    pyplot.title('PR curve')
    pyplot.xlabel('recall\n\n'
                  f'AUC = {auc_}\n')

    pyplot.tight_layout()
    pyplot.savefig(f'results/pr_curve')

    df = DataFrame(array([p, r]).T, columns=['p', 'r'])
    df.to_csv(f'results/pr.csv', index=False)


def prediction_bias(y_pred, target, label_names, threshold=0.5):
    y_pred = cast(greater(y_pred, threshold), float32)
    y_true = cast(target, float32)

    y_pred_avg = mean(y_pred, axis=0)
    y_true_avg = mean(y_true, axis=0)

    bias = y_pred_avg / y_true_avg * 100 - 100

    style.use('bmh')
    pyplot.figure(figsize=(8, 8))
    yticks = range(len(label_names))
    width = .24
    pyplot.barh([x + .5 * width for x in yticks], y_true_avg, width, label='true')
    pyplot.barh([x - .5 * width for x in yticks], y_pred_avg, width, label='prediction')
    pyplot.ylabel('label')
    pyplot.title(f'Prediction Bias')
    pyplot.yticks(yticks, label_names)

    offset = pyplot.xticks()[0]
    offset = .025 * (offset[1] - offset[0])
    for i, (b, x) in enumerate(zip(bias, [max(p, t) for p, t in zip(y_pred_avg, y_true_avg)])):
        pyplot.text(x + offset, yticks[i] - .75 * width, f'{b:+.1f}%', fontsize='small')

    bias = math.abs(bias)
    bias = reduce_mean(bias)
    pyplot.xlabel('Average occurrence\n\n'
                  f'Average bias: {bias:.1f}%')

    pyplot.tight_layout()
    pyplot.legend()
    pyplot.savefig(f'results/prediction_bias')


def confusion_matrix(y_pred, target, label_names, threshold=0.5):
    y_pred = cast(greater(y_pred, threshold), int32)
    y_true = cast(target, int32)
    cm = math.confusion_matrix(y_true, y_pred)
    cm = DataFrame(cm, index=label_names, columns=label_names)
    print(cm)


def perf_grid(y_pred_val, target, label_names, n_thresh=100):
    y_val = target
    label_freq = target.sum(axis=0)
    label_index = [i for i in range(len(label_names))]
    thresholds = linspace(0, 1, n_thresh + 1).astype(npfloat32)

    ids, labels, freqs, tps, tns, fps, fns, precisions, recalls, f1s = [], [], [], [], [], [], [], [], [], []
    for x in label_index:
        for thresh in thresholds:
            ids.append(x)
            labels.append(label_names[x])
            freqs.append(round(label_freq[x] / len(y_val), 2))
            y_pred = y_pred_val[:, x]
            y_true = y_val[:, x]
            y_pred = y_pred > thresh
            tp = count_nonzero(y_pred * y_true)
            tn = count_nonzero((1 - y_pred) * (1 - y_true))
            fp = count_nonzero(y_pred * (1 - y_true))
            fn = count_nonzero((1 - y_pred) * y_true)
            p = tp / (tp + fp + 1e-16)
            r = tp / (tp + fn + 1e-16)
            f1 = 2 * tp / (2 * tp + fn + fp + 1e-16)
            tps.append(tp)
            tns.append(tn)
            fps.append(fp)
            fns.append(fn)
            precisions.append(p)
            recalls.append(r)
            f1s.append(f1)

    grid = DataFrame({
        'id': ids,
        'label': labels,
        'freq': freqs,
        'threshold': list(thresholds) * len(label_index),
        'tp': tps,
        'tn': tns,
        'fp': fps,
        'fn': fns,
        'precision': precisions,
        'recall': recalls,
        'f1': f1s})

    grid = grid[['label', 'freq', 'threshold',
                 'tp', 'tn', 'fp', 'fn', 'precision', 'recall', 'f1']]
    grid.to_csv('results/perf_grid.csv', index=False)

    grid = grid.groupby(['label']).max().sort_values('f1', ascending=False).reset_index()
    grid.to_csv('results/perf_grid_grouped.csv', index=False)

    return grid


def show_prediction(movie, model, classes):
    img = load_img(f'data/images/w342/{movie[1]}.jpg', target_size=(IMAGE_WIDTH, IMAGE_HEIGHT, IMAGE_CHANNELS))
    img = img_to_array(img) / 255
    img = expand_dims(img, axis=0)
    prediction = (model.predict(img) > 0.5).astype('int')
    prediction = Series(prediction[0])
    prediction.index = classes
    p_1 = prediction[prediction == 1].index.values
    p_0 = prediction[prediction == 0].index.values

    # print(f'{movie[1]:<6}\tPrediction: {prediction.tolist()}')
    # print(f'      \tTarget:     {movie[2]}')
    # print(f'      \tJaccard:    {jaccard:.2f}')

    p_1 = len(intersect1d(p_1, movie[2])) / len(set(p_1).union(movie[2]))
    p_0 = len(intersect1d(p_0, movie[2])) / len(set(p_0).union(movie[2]))

    return 0.5 * (p_1 + p_0)
