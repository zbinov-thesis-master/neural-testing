from collections import Counter, OrderedDict
from json import loads
from pathlib import Path

import numpy.random
import pandas
from matplotlib import pyplot
from numpy import intersect1d
from pandas import read_csv, Series


def plot_set_style(ax_):
    ax_.spines['top'].set_color('#DDDDDD')
    ax_.spines['bottom'].set_color('#DDDDDD')
    ax_.spines['right'].set_color('#DDDDDD')
    ax_.spines['left'].set_color('#DDDDDD')
    ax_.set_axisbelow(True)
    ax_.set_facecolor('#DDDDDD')
    pyplot.tight_layout()
    pyplot.grid(axis='x', c='w')


def plot_show_and_clear(filepath: str = None):
    if filepath is not None:
        pyplot.savefig(filepath, dpi=300, bbox_inches='tight')
    else:
        pyplot.show()
    pyplot.clf()


TARGET_CLASS_COUNT = 4_000  # 4_000
TARGET_POWERSET_COUNT = None  # 2_000
CUTOFF_BOT = 4_00
CUTOFF_TOP = 9_0000
MIN_NO_OF_LABELS = 1
MIN_NO_OF_COMBINATION_OCCURRENCES = 5 if TARGET_POWERSET_COUNT is None else 1_000

USE_NAMES = True
BASE_DATA_DIR = f'data'
RESULTS_DIR = f'results'

genres_dict = {
    12: 'Adventure',
    14: 'Fantasy',
    16: 'Animation',
    18: 'Drama',
    27: 'Horror',
    28: 'Action',
    35: 'Comedy',
    36: 'History',
    37: 'Western',
    53: 'Thriller',
    80: 'Crime',
    99: 'Documentary',
    878: 'Science Fiction',
    9648: 'Mystery',
    10402: 'Music',
    10749: 'Romance',
    10751: 'Family',
    10752: 'War',
    10770: 'TV Movie'
}

### load dataset ##################################################################################
ds = read_csv(f'{BASE_DATA_DIR}/movies.csv', usecols=['id', 'genres'])
ds = ds[(ds['id'] != 413644) & (ds['id'] != 70843)]  # images for these are corrupted
ds['genres'] = ds['genres'].apply(loads)
print(f'# of records before:\t{len(ds)}')

### count class occurrences #######################################################################
genres_count = Counter()
ds['genres'].apply(lambda x: genres_count.update(x))
genres_count = OrderedDict(sorted(genres_count.items(), key=lambda x: x[1]))

### plot ##########################################################################################
_, ax = pyplot.subplots()
if USE_NAMES:
    ax.barh(list(map(lambda x: genres_dict[x], genres_count.keys())), genres_count.values())
else:
    ax.barh(list(map(lambda x: str(x), genres_count.keys())), genres_count.values())
for i, v in enumerate(genres_count.values()):
    ax.text(v, i - 0.15, f' {v}', alpha=0.9, fontsize='x-small')
ax.axvline(CUTOFF_BOT, c='r', alpha=0.3)
ax.text(CUTOFF_BOT, -0.85, f' {CUTOFF_BOT}', c='r', alpha=0.6, fontsize='x-small')
if CUTOFF_TOP < max(genres_count.values()):
    ax.axvline(CUTOFF_TOP, c='r', alpha=0.3)
    ax.text(CUTOFF_TOP, -0.85, f' {CUTOFF_TOP}', c='r', alpha=0.6, fontsize='x-small')
pyplot.title('Number of occurrences of genres')
pyplot.xlabel('# of occurrences')
pyplot.ylabel('genre')
plot_set_style(ax)

Path('results').mkdir(parents=True, exist_ok=True)
plot_show_and_clear(f'{RESULTS_DIR}/_genres_occurrences')

### remove too frequent and too infrequent genres from dataset ####################################
genres_acceptable = {k: genres_dict[k] for k, v in genres_count.items() if CUTOFF_BOT < v < CUTOFF_TOP}
ds['genres'] = ds['genres'].apply(lambda x: intersect1d(x, list(genres_acceptable.keys())))

###
if TARGET_CLASS_COUNT is not None:
    gather_genres = {k: ds[ds['genres'].apply(lambda x: k in x)]['id'].values for k in genres_dict.keys()}
    numpy.random.seed(1337)
    gather_genres = {k: numpy.random.choice(v, TARGET_CLASS_COUNT, False)
                     for k, v in gather_genres.items() if len(v) > TARGET_CLASS_COUNT}
    data = dict()
    for k, v in gather_genres.items():
        for e in v:
            try:
                data[e].append(k)
            except KeyError:
                data[e] = [k]
    data = numpy.array([numpy.array([k, tuple(v)], dtype=numpy.object) for k, v in data.items()])
    ds = pandas.DataFrame(data, index=None, columns=ds.columns)

### remove too infrequent genres combinations from dataset ########################################
combinations_counter = Counter(tuple(c) for c in ds['genres'])
combinations_counter = OrderedDict(filter(lambda x: len(x[0]) >= MIN_NO_OF_LABELS, combinations_counter.items()))
combinations_counter = OrderedDict(filter(lambda x: x[1] >= MIN_NO_OF_COMBINATION_OCCURRENCES, combinations_counter.items()))
combinations_counter = OrderedDict(sorted(combinations_counter.items(), key=lambda x: -x[1]))
ds['genres'] = ds['genres'].apply(lambda x: tuple([e for e in x]))
ds = ds[ds['genres'].isin([c for c in combinations_counter.keys()])]

###
if TARGET_POWERSET_COUNT is not None:
    data = dict()
    numpy.random.seed(1337)
    for k, v in combinations_counter.items():
        if v < MIN_NO_OF_COMBINATION_OCCURRENCES:
            continue
        indices = numpy.random.choice(ds[ds['genres'] == k]['id'].values, min(v, TARGET_POWERSET_COUNT), False)
        for i in indices:
            data[i] = k
    data = numpy.array([numpy.array([k, v], dtype=numpy.object) for k, v in data.items()])
    ds = pandas.DataFrame(data, index=None, columns=ds.columns)

### prune rows with no genres left ################################################################
ds = ds[ds['genres'].apply(len) != 0]
print(f'# of records after: \t{len(ds)}')

### save the results ##############################################################################
ds['genres'] = ds['genres'].apply(lambda x: [e for e in x])
ds.to_csv(f'{BASE_DATA_DIR}/movies-clean.csv', index=False)

genres_left = numpy.unique([y for x in ds['genres'].values for y in x])
genres_acceptable = {k: v for k, v in genres_acceptable.items() if k in genres_left}
Series(genres_acceptable).to_csv(f'{BASE_DATA_DIR}/genres_acceptable.csv', header=False)

###
genres_count = Counter()
ds['genres'].apply(lambda x: genres_count.update(x))
genres_count = OrderedDict(sorted(genres_count.items(), key=lambda x: -x[1]))
print(genres_count)

combinations_counter = Counter(tuple(c) for c in ds['genres'])
combinations_counter = OrderedDict(filter(lambda x: len(x[0]) >= MIN_NO_OF_LABELS, combinations_counter.items()))
combinations_counter = OrderedDict(filter(lambda x: x[1] >= MIN_NO_OF_COMBINATION_OCCURRENCES, combinations_counter.items()))
combinations_counter = OrderedDict(sorted(combinations_counter.items(), key=lambda x: -x[1]))
cci = combinations_counter.items() if TARGET_POWERSET_COUNT is None else combinations_counter.items()
print(cci, '\n', len(cci))

print(ds.shape)
