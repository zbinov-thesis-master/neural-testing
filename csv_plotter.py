from pathlib import Path

from pandas import read_csv, concat
from matplotlib import pyplot, style

COLORS = ['b', 'r', 'c', 'm', 'y', 'g']
BASE_RESULTS_DIR = f'P:/meng/pretrained_powerset/double_f1'
OUTPUT_RESULTS_DIR = f'{BASE_RESULTS_DIR}/_combined'
Path(OUTPUT_RESULTS_DIR).mkdir(parents=True, exist_ok=True)

DIRS = ['001',
        '002',
        '003',
        '004',
        '005']
SIZE = 8


def load_data(paths):
    data = []
    for i, path in enumerate(paths):
        data.append(read_csv(path,
                             names=[f'{i}_0', f'{i}_1'], skiprows=1,
                             index_col=None))
    return data


def plot_metric(data, metric_name):
    style.use('bmh')
    pyplot.figure(figsize=(SIZE, SIZE))

    for i, (dir_, data_) in enumerate(zip(DIRS, data)):
        pyplot.plot(range(1, len(data_) + 1), data_[f'{i}_0'],
                    f'{COLORS[i]}--', alpha=.5, label=f'{dir_}, training')
        pyplot.plot(range(1, len(data_) + 1), data_[f'{i}_1'],
                    f'{COLORS[i]}', alpha=.5, label=f'{dir_}, validation')

    pyplot.legend()
    pyplot.ylabel(metric_name)
    pyplot.xlabel('epoch')

    pyplot.tight_layout()
    pyplot.savefig(f'{OUTPUT_RESULTS_DIR}/{metric_name}')
    pyplot.close()


def plot_roc(data):
    style.use('bmh')
    pyplot.figure(figsize=(SIZE, SIZE))

    pyplot.plot([0, 1], [0, 1], 'k:', alpha=.2)
    for i, (dir_, data_) in enumerate(zip(DIRS, data)):
        pyplot.plot(data_[f'{i}_0'], data_[f'{i}_1'],
                    f'{COLORS[i]}', alpha=.5, label=f'{dir_}')

    pyplot.xlim(-.05, 1.05)
    pyplot.ylim(-.05, 1.05)

    pyplot.legend()
    pyplot.ylabel('TP rate')
    pyplot.xlabel('FP rate')

    pyplot.tight_layout()
    pyplot.savefig(f'{OUTPUT_RESULTS_DIR}/roc')
    pyplot.close()


def plot_pr(data):
    style.use('bmh')
    pyplot.figure(figsize=(SIZE, SIZE))

    for i, (dir_, data_) in enumerate(zip(DIRS, data)):
        pyplot.plot(data_[f'{i}_1'], data_[f'{i}_0'],
                    f'{COLORS[i]}', alpha=.5, label=f'{dir_}')

    pyplot.xlim(-.05, 1.05)
    pyplot.ylim(-.05, 1.05)

    pyplot.legend()
    pyplot.ylabel('precision')
    pyplot.xlabel('recall')

    pyplot.tight_layout()
    pyplot.savefig(f'{OUTPUT_RESULTS_DIR}/pr')
    pyplot.close()


def main():
    missing = []
    for metric in ['loss', 'macro_double_f1', 'accuracy', 'balanced_accuracy', 'double_precision', 'double_recall',
                   'aucroc', 'aucpr', 'cohen_kappa', 'f1_score_macro', 'f1_score_micro',
                   'fbeta_score_macro', 'fbeta_score_micro', 'hamming_loss', 'contrastive_loss',
                   'true_positive_rate', 'true_negative_rate', 'positive_predictive_value', 'negative_predictive_value',
                   'false_negative_rate', 'false_positive_rate', 'false_discovery_rate', 'false_omission_rate',
                   'threat_score', 'fowlkes_mallows_index', 'matthews_correlation', 'informedness', 'markedness',
                   'macro_f1']:
        try:
            data = load_data([f'{BASE_RESULTS_DIR}/{x}/{metric}.csv' for x in DIRS])
            plot_metric(data, metric)

            print(metric)
            print(concat(data, axis=1).to_string())
        except FileNotFoundError:
            missing.append(metric)

    try:
        data = load_data([f'{BASE_RESULTS_DIR}/{x}/roc.csv' for x in DIRS])
        plot_roc(data)
    except FileNotFoundError:
        missing.append('roc')

    try:
        data = load_data([f'{BASE_RESULTS_DIR}/{x}/pr.csv' for x in DIRS])
        plot_pr(data)
    except FileNotFoundError:
        missing.append('pr')

    if missing:
        print('\nSome items could not be found:', missing)


main()
